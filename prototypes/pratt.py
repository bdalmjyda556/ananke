# see http://effbot.org/zone/simple-top-down-parsing.htm

import re

# operator, left_bp, nud_bp, led_bp, led_dyad (0: postfix 1: infix)
# -1 means "no value", or something like "this has no nud/led"
bptab = {
    #tok    lbp  nud  led infix?
    'end': [  0,   0,   0,   0], # EOF token
    'lit': [  0,   0,   0,   0], # literal numbers
    'name':[  0,   0,   0,   0], # identifiers
    '=':   [  5,  -1,   5,   1], # assignment
    '+':   [ 10,  10,  10,   1], # addition and "+x"
    '-':   [ 10,  10,  10,   1], # subtraction and negation
    '*':   [ 20,  -1,  20,   1], # multiplication
    '/':   [ 20,  -1,  20,   1], # division
    '^':   [ 30,  -1,  29,   1], # right-associative power
    '!':   [ 30,  -1,  30,   0], # postfix factorial
    '[':   [ 40,  -1,   0,   1], # brackets, braces and parens
    '(':   [ 40,  -1,   0,   1],
    '{':   [ 40,  -1,  -1,   1],
    ']':   [  0,  -1,  -1,   0],
    ')':   [  0,  -1,   0,   0],
    '}':   [  0,  -1,   0,   0],
}

def led(left):
    global token
    t = token
    token = next() # pre eat it, every branch would have to do token=next() anyway
    if bptab[t[0]][3] == -1: # if no led
        raise SyntaxError("operator %r has no led"%t)
    elif (t[0] in "[("):
        right = expr(0)
        token = next() # eat closing ] or )
        return [t[0],left,right]
    elif bptab[t[0]][3] == 1: # we have an infix operator
        right = expr(bptab[t[0]][2])
        return [t[0], left, right]
    else: # we have a postfix operator
        return [t[0], left]

def nud():
    global token
    t = token
    token = next() # pre eat it, every branch would have to do token=next() anyway
    if (t[0] == "lit"):
        return t[1]
    elif (t[0] == "name"):
        return t[1]
    elif (t[0] in "[("):
        temp = expr(0)
        token = next() # eat closing ] or )
        return temp
    elif bptab[t[0]][1] == -1:
        raise SyntaxError("operator %r has no nud"%t)
    return [t[0],expr(bptab[t[0]][1])]

def parse_body():
    global token
    if (token[0] != '{'):
        raise SyntaxError("expected {")
    token = next()
    body = []
    while (token[0] != "}"):
        if token[0] == 'end':
            raise SyntaxError("expected }, but got to end of input")
        body.append(expr(0))

    token = next()
    return ['body',body]

def expr(rbp):
    global token

    left = []
    if token[0] == "name" and token[1] == "if":
        token = next()
        left = ["branch", [expr(0), parse_body()]]

        while token[0] == "name" and token[1] == "elif":
            token = next()
            left.append([expr(0),parse_body()])

        if token[0] == "name" and token[1] == "else":
            token = next()
            left.append([None,parse_body()])
    else:
        left = nud()

    while rbp < bptab[token[0]][0] and bptab[token[0]][2] != -1:
        #rbp < lbp and operator is infix
        left = led(left)

    return left

def parse(program):
    global token, next
    next = tokenize(program).__next__
    token = next()
    print(program, "->", expr(0))

def tokenize(program):
    for number, name, operator in re.findall("\s*(?:(\d+)|(\w+)|(.))", program):
        if number:
            yield ["lit", int(number)]
        elif name:
            yield ["name", name]
        elif operator in bptab:
            yield [operator]
        else:
            raise SyntaxError("unknown token: %r" % operator)
    yield ["end"]

parse("2^a[6+4]")
parse("f(2)^f(3)")
parse("1+2*3")
parse("1*2+3")
parse("-1--1")
parse("-1^-2^-3")
parse("1+2+3+4")
parse("8!*-3+-4")
parse("a = if a{b} + 9")
