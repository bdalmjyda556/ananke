#include <stdint.h>

typedef struct type {
	const char *name;
	enum{
		a_enum, a_struct, a_set,
		a_alias, a_deriv, a_ptr,
		a_fn,
		a_arr
	} kind;
	union {
		struct { struct type **members; } a_enum;
		struct { struct type **members; } a_struct;
		struct { struct type **members; } a_set;
		struct { struct type *origin; } a_alias;
		struct { struct type *origin; } a_deriv;
		struct { struct type *origin; } a_ptr;
		struct { struct type **in, **out; } a_fn;
		struct { 
			struct type *origin;
			enum {a_hashtable, a_btree, a_vector, a_matrix, a_dynarr} kind;
		} a_arr;
	};
	uint32_t qualifiers;
	void *predicates[];
} Type;

typedef struct value {
	Type *type;
	void *data;
} Value;

typedef struct variable {
	const char *name;
	Value val;
} Variable;

typedef struct scope {
	struct scope *parent;
	Type **types;
	Variable **variables;
} Scope;
