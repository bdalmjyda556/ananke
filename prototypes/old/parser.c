#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "string.c"
#include "table.c"
#include "array.c"
#include "utils.c"
#include "tokenizer.c"

struct symbol_scope_info {};
struct value_scope_info {};
// etc

struct scope_symtab_v {
	string s;
	enum {sym_type, sym_op_pre, sym_op_post, sym_op_in, sym_var, sym_fn, sym_co} kind;
	union {
		struct symbol_scope_info *sym_info;
		struct value_scope_info *val_info;
	}; // variant record :csgrad:
};

typedef struct scope_struct {
	struct scope_struct *parent;
	table symtab; // scope_symtab_v
} Scope;

typedef struct ast_struct {
	struct ast_struct *parent;
	struct ast_struct *children; // array of children, some slots may be unused, if so, NULL
	size_t numChildren;
	size_t parentChild; // this = this.parent->children[parentChild]
	enum {
		ast_fn_decl, // fn f<x>(y){z} 
		// children[0] = (ident)f
		// children[1] = (decl_list)x
		// children[2] = (decl_list)y
		// children[3] = (body)z
		ast_decl_list, // #children = however many args, [?] = (decl)?
		ast_decl,      // [0]=type, [1]=ident
		ast_decl_asgn, // [0]=type [1]=ident [2]=assignment
		ast_body,      // #children = however many expressions
		ast_op_in,     // infix operation, 3 children (expr, op, expr)
		ast_op_pre,    // 2 children (op, expr)
		ast_op_post,   // 2 children (expr, op)
		ast_op_asgn,   // composite assignment (x op= y) chidren{expr, op, expr}
		ast_for,       // for x in y {z}, children = {x,y,z}
		ast_do,        // do x {y}, children = {x,y}
		ast_call,      // f(x), children = {f,x}, where x is ast_call_args
		ast_call_args, // fn call args, #children = however many args
		ast_label,     // c[0] is ident
		ast_value,     // a literal value
		ast_type,      // a type identifier
		ast_var,       // any other identifier
		ast_use,       // use x.y.z: children = {x,y,z}
		ast_arr,       // [a,b,c] children = {a,b,c}
	} kind;
} AST;

/*
type_component: qualifier* ident|'[]'|'['expr']'|'[.'type']'|'[#'type']'|'<expr_list>'|'*'
compount_type: {decl (','|'/'|'|' decl)*}
type: type_component+ | compound_type
body: expr*
expr_list: expr (',' expr)*
decl_list: decl (',' decl)*
ident_list: ident (',' ident)*

decl: type|'var'|'let' ident
decl_assign: decl '=' expr
use_clause: use ident ('.'ident)* ('.*')?
for_clause: 'for' ident_list 'in' expr '{' body '}'
fn_clause: 'fn' ident ('<' decl_list '>')? '(' decl_list? ')' type? '{' body '}'
do_clause: 'do' count '{' body '}'
fn_call: ident '(' expr_list ')'
return: return expr_list
array: '[' expr_list ']'
operation: expr oper expr | expr oper | oper expr

expr:
	| '('expr')'
	| use_clause
	| fn_clause
	| do_clause
	| for_clause
	| decl_assign
	| decl
	| fn_call
	| return 
	| array
	| operation
	| ident
*/


#ifdef parsig
#elifdef parstart
#elifdef acceptTok
#elifdef optionalTok
#elifdef acceptSub
#elifdef optionalSub
#elifdef parsfail
#error these symbols can not be defined (jump to line)
#endif

const char *indent = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
#define parsig size_t n, Lexeme l[n], AST *context, size_t pdepth
#define parstart size_t tmp, last, i=0
#define parsucc return i
#define parsfail return 0
#define parestore i=last
#define parsave last=i
#define acceptTok(t) (printf("\n%.*s[%d] trying tok '%s' from L%d:", pdepth, indent, i, #t, __LINE__),(i<n && l[i++].token == t_##t) && printf(" [ok %d %s]", i, #t) || (printf(" [fail %s]",#t),0))
#define optionalTok(t) (printf("\n%.*s[%d] trying tok '%s' from L%d:", pdepth, indent, i, #t, __LINE__),(i<n && (l[i++].token == t_##t && printf(" [ok %d %s]", i, #t) || (printf(" [fail %s]",#t),i--,1) ) ))
#define acceptSub(f) (printf("\n%.*s[%d] trying sub '%s' from L%d:", pdepth, indent, i, #f, __LINE__),(i<n && (i += (tmp=parse_##f(n-i, &l[i], context, pdepth+1)), tmp) && printf(" [ok %d %s]",i,#f) || (printf(" [fail %s]",#f),0)))
#define optionalSub(f) (printf("\n%.*s[%d] trying sub '%s' from L%d:", pdepth, indent, i, #f, __LINE__),(i<n && (i += parse_##f(n-i, &l[i], context, pdepth+1)) && printf(" [ok %d %s]", i, #f) || (printf(" [fail %s]",#f),1)))
#define or || (i-=1,0) ||
// parser type signature

typedef struct parser_state{
	struct {
		size_t head;
		size_t len;
		Lexeme *arr;
	} lex;
	struct {
		AST *root;
		AST *head;
	} ast;
} Parstate;

void Parserror(Parstate *ps, const char *msg){
	Lexeme head = ps->lex.arr[ps->lex.head];
	printf("%.*s:%zu:%zu near %.*s: %s", head.loc.filename.len, head.loc.filename.str, head.loc.line, head.loc.column, head.str.len, head.str.str, msg);
	exit(0);
}

int parseToken(Parstate *ps, Token tok){
	if(ps->lex.arr[ps->lex.head].token == tok){
		ps->lex.head++;
		return 1;
	}
	return 0;
}

size_t parse_type(parsig);
size_t parse_expr(parsig);
size_t parse_decl_list(parsig);
size_t parse_body(parsig);
size_t parse_use(parsig);
size_t parse_decl(parsig);
size_t parse_fncall(parsig);
size_t parse_expr_list(parsig);
size_t parse_fn(parsig);
size_t parse_do(parsig);
size_t parse_for(parsig);
size_t parse_ident_list(parsig);
size_t parse_label(parsig);

size_t parse_term(parsig);
size_t parse_factor(parsig);
size_t parse_term(parsig);


size_t parse_expr(parsig){
	parstart;parsave;
	if (acceptTok(string_lit)){parsucc;}else{parestore;}
	if (acceptTok(int_lit)){parsucc;}else{parestore;}
	if (acceptSub(fncall)){parsucc;}else{parestore;}
	if (acceptSub(decl)){parsucc;}else{parestore;}
	if (acceptSub(label)){parsucc;}else{parestore;}
	if (acceptTok(ident)){parsucc;}else{parestore;}
	if (acceptSub(use)){parsucc;}else{parestore;}
	if (acceptSub(fn)){parsucc;}else{parestore;}
	if (acceptSub(do)){parsucc;}else{parestore;}
	if (acceptSub(for)){parsucc;}else{parestore;}
	if (acceptTok(lbrace)&&acceptSub(body)&&acceptTok(rbrace)){parsucc;}else{parestore;}
	parsfail;
}

size_t parse_label(parsig){
	parstart;
	if(acceptTok(ident) && acceptTok(colon)){
		parsucc;
	}
	parsfail;
}

size_t parse_fncall(parsig){
	parstart;
	if (acceptTok(ident) && acceptTok(lparen) && optionalSub(expr_list) && acceptTok(rparen)){
		parsucc;
	}
	parsfail;
}

size_t parse_body(parsig){
	parstart;
	if(acceptSub(expr)){
		while(1){
			if(!acceptSub(expr))
				break;
		}
		parsucc;
	}
	parsfail;
}

size_t parse_expr_list(parsig){
	parstart;
	// exp
	if (acceptSub(expr)){
		// , exp
		while(acceptTok(comma) && acceptSub(expr));
		parsucc-1;
	}
	parsfail;
}

size_t parse_ident_list(parsig){
	parstart;
	// exp
	if (acceptTok(ident)){
		// , exp
		while(1){
			parsave;
			if(acceptTok(comma) && acceptTok(ident)){
			}else{
				parestore;
				parsucc;
			}
		}
	}
	parsfail;
}

size_t parse_for(parsig){
	parstart;
	if ( acceptTok(for)
		&& acceptSub(ident_list)
		&& acceptTok(in)
		&& acceptSub(expr)
		&& acceptTok(lbrace)
		&& acceptSub(body)
		&& acceptTok(rbrace)
	){
		parsucc;
	}
	parsfail;
}

size_t parse_use(parsig){
	parstart;
	// use x
	if (acceptTok(use) && acceptTok(ident)){
		// .y
		while(acceptTok(dot) && acceptTok(ident));

		// optional .*
		parsave;
		if(acceptTok(dot) && acceptTok(star)){
			parsucc;
		} else {
			parestore;
			parsucc;
		};
	}
	parsfail;
}
size_t parse_do(parsig){
	parstart;
	if (
		acceptTok(do) &&
		optionalSub(expr) &&
		acceptTok(lbrace) &&
		acceptSub(body) &&
		acceptTok(rbrace)
	){
		parsucc;
	}
	parsfail;
}
size_t parse_fn(parsig){
	AST child = {0};

	size_t i=0, tmp;
	if (acceptTok(fn) && 
		acceptTok(ident) &&
		acceptTok(lparen) &&
		optionalSub(decl_list) &&
		acceptTok(rparen) &&
		acceptTok(ident) &&
		acceptTok(lbrace) &&
		optionalSub(body) &&
		acceptTok(rbrace)
	){
		return i;
	}
	return 0;
}

size_t parse_decl(parsig){
	parstart;
	if((acceptTok(let) or acceptTok(var)) && acceptTok(ident)){
		parsave;
		if(acceptTok(assign) && acceptSub(expr)){
			parsucc;
		}
		parestore;
		parsucc;
	}
	parsfail;
}

size_t parse_decl_list(parsig){
	parstart;
	if (acceptSub(decl)){
		while(1){
			parsave;
			if (acceptTok(comma) && acceptSub(decl)){
			} else {
				parestore;
				parsucc;
			}
		}
	}
	parsfail;
}

size_t parse_type(parsig){
}

void parse_and_free(LexemeArr l){
	for (size_t i=0; i<l.len; i++){
		printf("[%02d] %02d « %.*s »\n", i, l.arr[i].token, (int)l.arr[i].str.len, l.arr[i].str.str);
	}
	parse_body(l.len, l.arr, NULL, 0);
	free(l.arr);
}
#ifndef MAIN
#define MAIN
int main(int argc, char *argv[]){

	if(argc != 2){
		puts("expected ./parser filename.ank");
		exit(1);
	}

	string src;
	readfile(&src.str, &src.len, argv[1]);
	parse_and_free(lex(src));
	free((void*)src.str);

// 	parse_and_free(lex(newstr(
// "fn main() i32 {"
// "	let a = 1"
// "	var v = 5"
// "	p:"
// "	printl(i)"
// "	for v,k in \"hi\" {"
// "		for x in {{{""}}} {{{o}}}"
// "	}"
// "}"
// 	)));
}
#endif
