

void test(){
	struct {
		string input;
		Token expected;
	} tests[] = {
		{{0, ""}, t_ident},
		{{1, "x"}, t_ident},
		{{2, "as"}, t_as},
		{{2, "to"}, t_to},
		{{3, "len"}, t_ident},
		{{3, "let"}, t_let},
		{{3, "lez"}, t_ident},
		{{3, "xor"}, t_xor},
		{{3, "XOR"}, t_xor},
		{{4, "void"}, t_void},
		{{4, "with"}, t_with},
		{{5, "doggy"}, t_ident},
		{{5, "false"}, t_false},
		{{5, "while"}, t_while},
		{{6, "awhile"}, t_ident},
		{{6, "return"}, t_return},
		{{6, "letlet"}, t_ident},
		{{7, "garbage"}, t_garbage},
		{{8, "volatile"}, t_volatile},
		{{9, "shitpants"}, t_ident},
		{{9, "returnnil"}, t_ident}
	};

	size_t numtests = sizeof(tests)/sizeof(tests[0]);
	for (size_t i=0; i<numtests; i++){
		Token t = whichKeyword(tests[i].input);
		fprintf(stderr, "\"%.*s\": %s\n", (int)tests[i].input.len, tests[i].input.str, t!=t_ident?"keyword":"symbol");
		assert(t == tests[i].expected);
	}
	puts("====[ Passed all tests ]====");
}