#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//#define puts(...)
//#define printf(...)

enum etok {
	num, plus, min, mul, frac, pow, end
};

struct {
	int lbp;
} bptab[] = {
	[num] = {900},
	[plus] = {10},
	[min] = {10},
	[mul] = {20},
	[frac] = {20},
	[pow] = {30},
	[end] = {0}
};

struct lexeme{
	enum etok tok;
	double value;
};

struct lexeme nextTok(){
	static struct lexeme tokens[] = {
		{num, 3.5}, {plus,0}, {num, 9}, {mul,0}, {num, 12}, {end,0}
	};
	static int i = 0;
	if (i == sizeof(tokens)/sizeof(tokens[0])){
		puts("exhausted - this should be unreachable");
		exit(1);
	}
	return tokens[i++];
}

double expr(int);

// prefix operator (or left operand)
// "nud"
double pratt_nud(struct lexeme l, double left){
	switch(l.tok){
		case num:
		return l.value;

		default:
		puts("Unsupported nud");
		return 0;
	}
}

// infix (or postfix) operator
// "led"
double pratt_led(struct lexeme l, double left){
	switch(l.tok){
		case plus:
		return left + expr(10);

		case mul:
		return left * expr(20);

		default:
		puts("Unsupported led");
		return 0;
	}
}

double expr(int rbp){
	struct lexeme l1 = nextTok();
	if (l1.tok == end){
		puts("eof");
		return 0;
	}
	double left = pratt_nud(l1, 0);

	struct lexeme l2 = nextTok();
	while (rbp < bptab[l2.tok].lbp){
		l1 = l2;
		l2 = nextTok();
		left = pratt_led(l1, left);
	}

	return left;
}

int main(){
	double x = expr(0);
	printf("%f", x);
}