#include <stdlib.h>
#include "tokenizer.c"

/*
PRATT PARSER
adapted from the following article: "http://web.archive.org/web/20180623173231/http://effbot.org/zone/simple-top-down-parsing.htm"

Since in Ananke there are no statements, only expressions, this is the whole parser. It's a precedence parser so that natural, infix mathematical notation can be used in code.

The parser is recursive. Most Pratt parsers have global values for tokenization, but this one doesn't. A pointer to the Lexer and symbol Table are passed down into each recursive call. It should work the same, except this design is threadsafe. Multiple parsers can be run concurrently, given they are jumpstarted with different Lexers and Tables.

Each call to a parselet returns a node for the abstract syntax tree.

In Pratt nomenclature, the "nud" or null denotation are tokens that stand on their own, i.e. prefix operators or operands. The "led" or left denotation have something to the left of them, so leds are always postfix or infix which includes the a[i] and f(x) brackets/"operators".

In the parselets:
table *c is the context (symbol table)
Lexer *l is the tokenizer "coroutine" state
*/

// abstract syntax tree
typedef struct AST {
	enum {k_error, k_decl, k_for_in, k_for_is, k_binop, k_unop, k_term, k_complit, k_block, k_call, k_expr, k_index, k_array, k_array_assoc} kind;
	Lexeme lexeme;        // which lexeme does this node represent
	struct AST *branches; // children of the node
	int num_children;     // amount of children (0 if leaf node)
	void *type;           // the Type signature (to be done)
} AST;

// debug function for printing out the AST
void AST_dump(AST a, int depth){
	const char *indent = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; // scuffed
	const char *kinds[] = {"erronous", "declaration", "for_in", "for_is", "bin op", "un op", "term", "comp lit", "block", "call", "expr", "index", "array", "assoc array"};
	printf("%.*s%s: ",depth,indent,kinds[a.kind]);print_lexeme(a.lexeme);
	for(int i=0; i<a.num_children; i++){
		AST_dump(a.branches[i], depth+1);
	}
}

// parses an expression
AST parse_expr(Lexer *l, Table *c, int rbp);

// prefix stuff
AST parse_nud(Lexer *l, Table *c, Lexeme x);

// infix and postfix stuff
AST parse_led(Lexer *l, Table *c, Lexeme x, AST left);

// for looking up the left binding power
int lbp(Token t);

// binding power table
static const struct {
	//Token tok;
	int left_bp; // -1 if terminal
	int nud_bp; // -1 if no nud
	int led_bp; // -1 if no led
	int infix_led;
} bptab[] = {
	[t_eof     ]={  0,   0,  -1, 0},
	[t_string  ]={ -1,   0,  -1, 0}, // ""
	[t_ident   ]={ -1,   0,  -1, 0}, // blabla
	[t_float   ]={ -1,   0,  -1, 0}, // 1.1
	[t_int     ]={ -1,   0,  -1, 0}, // 1
	[t_assign  ]={ 10,  -1,  10, 1}, // =
	[t_and     ]={ 20,  -1,  20, 1}, // and
	[t_or      ]={ 20,  -1,  20, 1}, // or
	[t_xor     ]={ 20,  -1,  20, 1}, // xor
	[t_not     ]={ 20,  20,  -1, 1}, // not
	[t_in      ]={ 30,  -1,  30, 1}, // in
	[t_comp    ]={ 30,  -1,  30, 1}, // <=>
	[t_lteq    ]={ 30,  -1,  30, 1}, // <=
	[t_gteq    ]={ 30,  -1,  30, 1}, // >=
	[t_neq     ]={ 30,  -1,  30, 1}, // !=
	[t_eq      ]={ 30,  -1,  30, 1}, // ==
	[t_lt      ]={ 30,  -1,  30, 1}, // <
	[t_gt      ]={ 30,  -1,  30, 1}, // >
	[t_plus    ]={ 40,  40,  40, 1}, // +
	[t_minus   ]={ 40,  40,  40, 1}, // -
	[t_concat  ]={ 40,  -1,  40, 1}, // ..
	[t_bor     ]={ 50,  -1,  50, 1}, // |
	[t_band    ]={ 60,  -1,  60, 1}, // &
	[t_tilde   ]={ 70,  -1,  70, 1}, // ~
	[t_lrot    ]={ 80,  -1,  80, 1}, // <<<
	[t_rrot    ]={ 80,  -1,  80, 1}, // >>>
	[t_lshift  ]={ 80,  -1,  80, 1}, // <<
	[t_rshift  ]={ 80,  -1,  80, 1}, // >>
	[t_mult    ]={ 80,  -1,  80, 1}, // *
	[t_mmult   ]={ 80,  -1,  80, 1}, // **
	[t_div     ]={ 80,  -1,  80, 1}, // /
	[t_div2    ]={ 80,  -1,  80, 1}, // /%
	[t_floordiv]={ 80,  -1,  80, 1}, // //
	[t_mod     ]={ 80,  -1,  80, 1}, // %
	[t_mod2    ]={ 80,  -1,  80, 1}, // %%
	[t_incr    ]={ 90,  -1,  90, 0}, // ++
	[t_decr    ]={ 90,  -1,  90, 0}, // --
	[t_pow     ]={ 90,  -1,  89, 1}, // ^
	[t_bnot    ]={ 90,  90,  -1, 0}, // !
	[t_as      ]={100,  -1, 100, 1}, // as
	[t_to      ]={100,  -1, 100, 1}, // to
	[t_coalesce]={110,  -1, 110, 1}, // ??
	[t_safe    ]={110,  -1, 110, 0}, // ?
	[t_addr    ]={110,  -1, 110, 0}, // $
	[t_deref   ]={110,  -1, 110, 0}, // @
	[t_dot     ]={110,  -1, 110, 1}, // .
	[t_lbracket]={110,   0,   0, 1}, // [
	[t_lparen  ]={110,   0,   0, 1}, // (
	[t_lbrace  ]={110,   0,   0, 1}, // {
	[t_rbracket]={ -1,  -1,  -1, 0}, // ]
	[t_rparen  ]={ -1,  -1,  -1, 0}, // )
	[t_rbrace  ]={ -1,  -1,  -1, 0}, // }
	[t_comma   ]={ -1,  -1,  -1, 0}, // ,
	[t_for     ]={110,   0,  -1, 0}, // for
};

AST parse_led(Lexer *l, Table *c, Lexeme x, AST left){
	if (bptab[x.token].led_bp == -1){
		printf("syntax error, this lexeme is not a postfix or infix operator: ");print_lexeme(x);
		exit(1);
	}
	else if(x.token == t_lbrace || x.token == t_lbracket || x.token == t_lparen){
		if (x.token == t_lbrace && l->curr.token==t_rbrace){
			AST *branches = malloc(sizeof(AST));
			branches[0] = left;
			lexer_consume(l,c);
			return (AST){k_complit, x,branches,1,NULL};
		}
		if (x.token == t_lbracket && l->curr.token == t_rbracket){
			AST *branches = malloc(sizeof(AST));
			branches[0] = left;
			lexer_consume(l,c);
			return (AST){k_index, x,branches,1,NULL};
		}
		if (x.token == t_lparen && l->curr.token == t_rparen){
			AST *branches = malloc(sizeof(AST));
			branches[0] = left;
			lexer_consume(l,c);
			return (AST){k_call, x,branches,1,NULL};
		}
			
		AST *branches = malloc(2*sizeof(AST));
		branches[0] = left;
		branches[1] = parse_expr(l,c,0);
		lexer_consume(l,c); // eat closing paren
		return (AST){x.token==t_lbrace?k_complit:x.token==t_lbracket?k_index:k_call, x, branches, 2, NULL};
	}
	else if(bptab[x.token].infix_led){
		AST *branches = malloc(2*sizeof(AST));
		branches[0] = left;
		branches[1] = parse_expr(l,c,bptab[x.token].led_bp);
		return (AST){k_binop,x,branches,2,NULL};
	}
	else{
		AST *branches = malloc(sizeof(AST));
		branches[0] = left;
		return (AST){k_unop,x,branches,1,NULL};
	}
}

AST parse_nud(Lexer *l, Table *c, Lexeme x){
	if (bptab[x.token].nud_bp == -1){
		printf("syntax error, this lexeme is not a prefix operator or terminal: ");print_lexeme(x);
		exit(1);
	}
	switch (x.token){
		case t_int:
		case t_float:
		case t_string:
		return (AST){k_term, x,NULL,0,NULL};

		case t_ident:{
			if (l->curr.token == t_ident && l->curr.pos.line == x.pos.line){
				// `ident ident` on the same line means a "typename varname" declaration
				// todo: infer if x is a type identifier by context
				AST *child = malloc(sizeof(AST));
				*child = (AST){.kind=k_term, .lexeme=l->curr, .branches=NULL, .num_children=0, .type=NULL};
				lexer_consume(l,c);
				return (AST){k_decl, x, child, 1, NULL};
			}
			// otherwise, just an ident
			return (AST){k_term, x,NULL,0,NULL};
		}
		case t_fn:{
			// fn name(args)ret {body}
			// where name is an identifier or nothing
			// where args takes the shape of decl,decl,decl...
			// where ret is a type or nothing
			// where body is of the form expr expr expr...
			AST name = {0};
			if(l->curr.token == t_ident){
				name = (AST){k_term, l->curr, NULL, 0, NULL};
				lexer_consume(l,c);
			}

			int num_args = 0;
			AST *args = NULL;
			if(l->curr.token == t_lparen){
				lexer_consume(l,c);

				while (l->curr.token != t_rparen && l->curr.token != t_eof){
					args = realloc(args, (++num_args)*sizeof(AST));
					args[num_args-1] = parse_expr(l,c,0);
					if (args[num_args-1].kind != k_decl){
						printf("error: a function argument needs to be a declaration, near ");
						goto fn_error;
					}
					if (l->curr.token == t_comma){
						lexer_consume(l,c);
					} else {
						break;
					}
				}

				if(l->curr.token != t_rparen){
					printf("expected a `)` to close the function arguments, instead got ");
					goto fn_error;
				}
				lexer_consume(l,c);
			} else {
				printf("error: expected a '(', instead got ");
				goto fn_error;
			}

			AST body, ret={0};
			if(l->curr.token == t_lbrace){
				body = parse_expr(l,c,0);
			} else {
				ret = parse_expr(l,c,0); // parse_type
				body = parse_expr(l,c,0);
			}

			int num_children = num_args+3;
			AST *children = malloc(num_children*sizeof(AST));
			children[0] = body;
			children[1] = name;
			children[2] = ret;
			if (args){
				memcpy(&children[3], args, num_args*sizeof(AST));
				free(args);
			}
			return (AST){k_decl, x, children, num_children, NULL};



			fn_error:
			print_lexeme(l->curr);
			puts("the correct function syntax is `fn name(args)ret{body}`. name and ret are optional and args may just be `()`.");
			exit(1);
		}
		// case t_var:
		// case t_let:{

		// }

		case t_lbrace:{
			int i = 0;
			int cap = 10;
			AST *xs = malloc(cap*sizeof(AST));
			while (l->curr.token != t_eof && l->curr.token != t_rbrace){
				xs[i++] = parse_expr(l,c,0);
				if (i == cap){
					cap*=2;
					xs = realloc(xs, cap*sizeof(AST));
				}
			}
			if (l->curr.token != t_rbrace){
				printf("error, expected '}' but instead got ");print_lexeme(l->curr);
				exit(1);
			};
			lexer_consume(l,c);
			return (AST){.kind = k_block, .lexeme = x, .branches=xs, .num_children=i, NULL};
		}

		case t_lparen:{
			AST inner = parse_expr(l,c,0);
			if (l->curr.token != t_rparen){
				printf("error, expected ')' but instead got ");print_lexeme(l->curr);
				exit(1);
			};
			lexer_consume(l,c);
			return inner;
		}

		case t_lbracket:{
			enum {ak_unknown, ak_assoc, ak_array} arr_kind = ak_unknown;
			int i = 0;
			int cap = 10;
			AST *xs = malloc(cap*sizeof(AST));
			while (l->curr.token != t_eof && l->curr.token != t_rbracket){
				xs[i++] = parse_expr(l,c,0);
				if (i+1 > cap){
					cap*=2;
					xs = realloc(xs, cap*sizeof(AST));
				}
				if (l->curr.token == t_comma){
					// apparently we have a normal array
					if (arr_kind == ak_assoc){
						printf("error: you can not mix normal arrays and associative arrays, near ");print_lexeme(l->curr);
						exit(1);
					}
					arr_kind = ak_array;
					lexer_consume(l,c);
				} else 
				if (l->curr.token == t_colon){
					// apparently we have an assoc array
					if (arr_kind == ak_array){
						printf("error: you can not mix associative arrays and normal arrays, near ");print_lexeme(l->curr);
						exit(1);
					}
					arr_kind = ak_assoc;
					lexer_consume(l,c);
					xs[i++] = parse_expr(l,c,0);
					if(l->curr.token != t_comma){
						break;
					}
					lexer_consume(l,c);
				} else {
					if (arr_kind == ak_assoc){
						printf("error, expected a colon followed by a value. You can not mix associate arrays and normal arrays near ");print_lexeme(l->curr);
						exit(1);
					}
					break;
				}
			}
			if (l->curr.token != t_rbracket){
				printf("error, expected ']' or `, next_item` but instead got ");print_lexeme(l->curr);
				exit(1);
			};
			lexer_consume(l,c);
			return (AST){.kind = ak_assoc?k_array_assoc:k_array, .lexeme = x, .branches=xs, .num_children=i, NULL};
		}
		case t_for:{
			// for x[,y,z,...] in xs {}
			// for x=0,1[,2] {}
			if(l->curr.token != t_ident){
				printf("error, expected an indentifier after `for`, instead got ");print_lexeme(l->curr);
				puts("The correct for-loop syntax is\n\
	1. for x[, y, z, ...] in xs {}\n\
	2. for x = start, end[, step] {}");
				exit(1);
			}
			AST iter_var = (AST){.kind=k_term, .lexeme=l->curr, NULL, 0, NULL};
			lexer_consume(l,c);
			if(l->curr.token == t_assign){
				// for x=start,end[,step] type loop
				lexer_consume(l,c);
				AST iter_start = parse_expr(l,c,0);
				if(l->curr.token != t_comma){
					printf("error, expected a comma after start. The correct syntax is `for x=start,end[,step] {something}`. Instead got ");print_lexeme(l->curr);
					exit(1);
				}
				lexer_consume(l,c);
				AST iter_end = parse_expr(l,c,0);
				AST iter_step;
				int has_step = 0;
				if(l->curr.token == t_comma){
					// step provided
					has_step = 1;
					lexer_consume(l,c);
					iter_step = parse_expr(l,c,0);
				}
				// and now finally, the actual body of the loop
				AST loop_body = parse_expr(l,c,0);
				int num_children = 1+1+1+has_step+1;
				AST *children = malloc(num_children*sizeof(AST));
				children[0] = loop_body;
				children[1] = iter_var;
				children[2] = iter_start;
				children[3] = iter_end;
				if(has_step){children[4] = iter_step;} // clever scheme, huh?
				return (AST){.kind = k_for_is, .lexeme=x, .branches=children, .num_children=num_children, NULL};
			}

			AST *iter_vars = NULL;
			int num_iter_vars = 0;
			while(l->curr.token == t_comma){
				// list of iterators
				// for x,y,z,?,?? in xs
				lexer_consume(l,c);
				if(l->curr.token == t_ident){
					num_iter_vars++;
					iter_vars = realloc(iter_vars, num_iter_vars*sizeof(AST));
					iter_vars[num_iter_vars-1] = (AST){.kind=k_term, .lexeme=l->curr, .branches=NULL, .num_children=0, .type=NULL};
					lexer_consume(l,c);
				} else {
					printf("error, expected an identifier (for x,y,z,... in xs) instead got ");print_lexeme(l->curr);
					exit(1);
				}
			}

			// what are we iterating?
			// for x[,y,z] in xs
			if(l->curr.token != t_in){
				printf("error, expected an `in` (for x,... IN xs) instead got ");print_lexeme(l->curr);
				exit(1);
			}
			lexer_consume(l,c);
			AST iteree = parse_expr(l,c,0);
			AST loop_body = parse_expr(l,c,0);

			int num_children = 1+num_iter_vars+1+1;
			AST *children = malloc(num_children*sizeof(AST));
			children[0] = loop_body;
			children[1] = iteree;
			children[2] = iter_var;
			memcpy(&children[3], iter_vars, sizeof(AST)*num_iter_vars);
			free(iter_vars);
			return (AST){.kind = k_for_in, .lexeme=x, .branches=children, .num_children=num_children, .type=NULL};
		}

		default:{
			AST *branches = malloc(sizeof(AST));
			branches[0] = parse_expr(l,c,bptab[x.token].nud_bp);
			return (AST){k_unop, x,branches,1,NULL};
		}
	}
}

AST parse_expr(Lexer *l, Table *c, int rbp){
	Lexeme t = l->curr;
	lexer_consume(l,c);
	if (t.token == t_eof){
		printf("warning: unfinished expression: ");print_lexeme(t);
		return (AST){k_error, t, NULL, 0, NULL};
	}
	AST left = parse_nud(l,c,t);

	// lbp < rbp means another will handle that operator
	// led_bp == -1 means not infix
	// an opening ([{ starting on a different line is never infix
	while (bptab[l->curr.token].left_bp > rbp
		&& bptab[l->curr.token].led_bp != -1
		&&!(l->curr.pos.line != t.pos.line
			&&(l->curr.token == t_lbracket
			|| l->curr.token == t_lbrace
			|| l->curr.token == t_lparen
		))
	){
		t = l->curr;
		lexer_consume(l,c);
		left = parse_led(l,c,t,left);
	}
	return left;
}

AST parse_prog(const char *prog, const char *name){
	Table context;
	tab_init(&context, 64);

	Lexer l = lex(prog, name);
	lexer_consume(&l,&context); // prime lexer for pratt parsing
	while (l.curr.token != t_eof){
		AST x = parse_expr(&l,&context,0);
		AST_dump(x,0);
	}
	return (AST){0};

	free(context.arr);
}
