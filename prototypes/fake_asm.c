#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char buf[512];
char *code = buf;
char *(retstack[32]) = {0}; // for returning from function calls
int retidx = 0;

typedef struct {
    union{
        char *str;
        char *sub; // points to location in code
        int num;   
    };
    enum{t_num, t_sub, t_str} type;
} fasm_value;

struct {
    char* key;
    fasm_value val;
} symtab [128];
int symcap = 128;
int symlen = 0;

int get(char *name, fasm_value *dest){
    for (int i = 0; i < symlen; ++i){
        if (strcmp(symtab[i].key, name)){
            continue;
        }
        *dest = symtab[i].val;
        return 1;
    }
    return 0;
}

int set(char *name, fasm_value val){
    if (symlen == symcap){
        puts("ERROR too many symbols defined - max 128");
        exit(1);
    }
    symtab[symlen].val = val;
    symtab[symlen].key = name;
    symlen++;
    return 1;
}

void gotta(int n, char *err){
    if (!n){
        printf("ERROR %s (got '%.*s'...)", err, 8, code);
        exit(1);
    }
}

void clear_space(){
    code += strspn(code, " \n");
}

int parse_name(char **dest){
    clear_space();
    int len = strspn(code, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_");
    if (len == 0){
        return 0;
    }
    *dest = calloc(1,len+1);
    memcpy(*dest, code, len);
    code += len;
    return 1;
}

int parse_keyword(char *key){
    char *try=NULL;
    if (!parse_name(&try)) return 0;
    int ret = 0==strncmp(try, key, strlen(try)); // key == try
    if (!ret){code -= strlen(try);} // undo parsing of name
    free(try);
    return ret;
}

int parse_num(int *dest){
    clear_space();
    int len = strspn(code, "0123456789");
    if(!len){return 0;}
    *dest = atoi(code);
    code += len;
    return 1;
}

int parse_str(char **dest){
    clear_space();
    if(*code != '"'){return 0;}
    code++;
    int len = strcspn(code, "\"");
    if (!len) return 0;
    *dest = calloc(1,len+1);
    memcpy(*dest, code, len);
    code += len+1;
    return 1;
}

int parse_include(){
    char *first=NULL, *second=NULL;
    int suc = parse_keyword("inc") && parse_name(&first) && parse_name(&second);
    if(suc){
        printf("including %s from %s!\n", second, first);
    }
    free(first);free(second);
    return suc;
}

int parse_value(fasm_value *dest){
    char *restore = code;
    char *s = NULL;
    if(parse_str(&s)){
        dest->type = t_str;
        dest->str = s;
        return 1;
    };
    int n;
    if(parse_num(&n)){
        dest->type = t_num;
        dest->num = n;
        return 1;
    }
    if(parse_name(&s)){
        if(!get(s, dest)){
            printf("ERROR, unknown variable '%s'\n",s);
        } else {
            return 1;
        }
    }
    code = restore;
    free(s);
    return 0;
}

int parse_put(){
    if(!parse_keyword("put")) return 0;
    char *str = NULL;
    if(parse_str(&str)){
        puts(str);
    };
    free(str);
    return 1;
}

void parse_prog();

int parse_repeat(){
    if(!parse_keyword("rpt"))return 0;
    int times = 0;
    gotta(parse_num(&times), "expected number");
    gotta(parse_keyword("bgn"), "expected bgn");
    char *top = code;
    while(times --> 0){
        code = top;
        parse_prog();
    }
    gotta(parse_keyword("end"), "expected end for the rpt");
    return 1;
}


char *safe_strstr(char *stack, char *needle){
    int len = strlen(needle);
    while(*stack){
        if(0==strncmp(stack, needle,len)){
            return stack;
        }
        stack++;
    }
    return stack;
}

int parse_sub(){
    if(!parse_keyword("sub")) return 0;
    char *name = NULL;
    gotta(parse_name(&name),"subroutine needs a name");
    gotta(parse_keyword("bgn"), "expected bgn");
    set(name, (fasm_value){.sub = code, .type=t_sub});
    
    int depth = 1;
    while (depth){
        if(parse_keyword("bgn")){
            depth++;
        } else if(parse_keyword("end")){
            depth--;
        } else {
            gotta(*code != '\0', "unterminated subroutine");
            code++;
        }
    }

    return 1;
}

int parse_ret(){
    if(!parse_keyword("ret")) return 0;
    if (retidx == 0){
        puts("ERROR stack underflow, there is nowhere to return");
        exit(1);
    }
    code = retstack[--retidx];
    parse_prog();

    return 1;
}

int parse_ent(){
    if(!parse_keyword("ent")) return 0;
    char *name;
    gotta(parse_name(&name), "which sub do you wish to enter");
    fasm_value val;
    gotta(get(name, &val)&&val.type==t_sub, "this subroutine does not exist");
    free(name);
    retstack[retidx++] = code;
    code = val.sub;
    parse_prog();

    return 1;
}

void parse_prog(){
    while(parse_include()||parse_put()||parse_repeat()||parse_sub()||parse_ret()||parse_ent());
}

int main(){
    //fgets(buf, 512, stdin);
    code =
"   sub one bgn"
"       put \"in one\""
"       ret"
"   end"
"   sub two bgn"
"       put \"in two\""
"       ent one"
"       ret"
"   end"
"   ent two"
"   put \"t\"";
    parse_prog();
    for (int i = 0; i < symlen; ++i){
        free(symtab[i].key);
    }
}