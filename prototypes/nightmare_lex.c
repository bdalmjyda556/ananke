#include <string.h>

typedef enum {l_none, l_eof, l_ident, l_keyw} lexeme;
static struct {const char *str; int len;} kws[] = {{"stock",5},{"bom",3}};
#define N_KWS (sizeof(kws)/sizeof(kws[0]))

typedef struct {
	char *head;
	int comment_depth;
	int word_len;
	enum {vibing, in_word, in_comment} state;
	int posskws[N_KWS]; // indices of the kws array which may be a match
	int numposskws;
	lexeme next;
	lexeme current;
} lextate;

lexeme lex1(lextate *l){
	if (l->state == in_word){
		if (*l->head == ' '){
			// set current and next
			l->current = l->next;
			if (l->numposskws &&
				l->word_len == kws[l->posskws[0]].len){
				l->next = l_keyw;
			} else {
				l->next = l_ident;
			}
			// reset posskws table
			for (int i = 0; i < N_KWS; ++i){
				l->posskws[i] = i;
			}
			l->numposskws = N_KWS;
			// reset state
			l->state = vibing;
			l->word_len = 0;
			// return lexeme
			return l_keyw;
		}
		for (int k=0; k < l->numposskws; k++){
			if (l->head[0] != kws[l->posskws[k]].str[l->word_len]){
				// remove this kwd from the considered list
				l->posskws[k] = l->posskws[l->numposskws];
				l->numposskws--;
			}
		}
	}
	l->head++;
	return l->current;
}

lextate lextart(char *stream){
	return (lextate){
		.head = stream,
		.comment_depth = 0,
		.word_len = 0,
		.state = vibing,
		.posskws = {0,0},
		.numposskws = 0,
		.next = l_none,
		.current = l_none,
	};
}

int main(){
	char *s = "bork bom stork stock stake stuck";
	lextate l = lextart(s);
	while (lex1(&l));
}