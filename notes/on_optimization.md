Optimization is always specialization, thus "general code" is antithetical to optimized code. But most code is made of "general code", like Rust' `Arc<T>` or C's `qsort()`.

# What you mean vs what you say

If you use `qsort`, what you mean is "I want this data but sorted", but what you're actually saying is "compile this specific sorting algorithm which uses a comparator callback". It's optimized in the completely wrong direction. It has flexibility and generality *baked in*. Sometimes quicksort isn't the fastest (in incremental construction algorithms, insertion sort can be faster, or what about radix sort if your data can be quantized into integer values?). Dynamic callbacks certainly aren't without overhead, and they forego all sorts of interesting compiler optimizations like inlining and constant propagation.

Even worse is `Arc<T>`, where what you mean is "Here's some data of T, I want to use it from multiple threads but one at a time, and I don't care about lifetimes/memory management". What you are actually saying however is very bleak: "Use this specific garbage collection algorithm (reference counting) which is not only high overhead in time and storage but also doesn't handle certain datastructures (ironic!), and I want all of that to be atomic, so lots of locks and unlocks". The compiler can't do jack shit with this, honestly. It's already "optimized" in the wrong direction. Truly optimal would have been if the compiler inferred the lifetime, inserted the malloc and free optimally, or heck even used "inferred stack region" based memory management (the latter not applicable to all multithreaded code), and emitted a simple "atomic" marker for reads and writes.

# Magic damage control

Indeed, the job of an optimizing compiler is mostly comprised of 2 things: on one hand it does micro-optimizations such as inlining a function, unrolling a loop, replacing `a*2` with `a>>1`. On the other hand it tries to desugar idioms, unravel abstractions and deflate bloat into actually sane code. To efficiently do the latter, a lot of the language must be "transparent" to the compiler. A good compiler is able to reason about the full semantics of `malloc()`, and not just think "ew, scary side effecty pointery stuff". That's why something like `malloc()` may not be the best idea if our first concern is speed: in fact a more "magic" language primitive like `new` might be better. 

This magic syntax however goes against a tendency in modern languages: homoiconicity. To not include a growable array primitive, but to rather make it "homo iconic" to user-made code, who only has access to templates rather than defining arbitrary syntax like `[]`. Thus, `vector<T>` is born. This also gives the illusion that everything of that language is implementable in the language itself. After all nothing is stopping you from writing your own `vector<T>` in C++.

On one hand, it's neat. On the other hand, it's pretty useless because when I'm building a project I'm not looking to reinvent/bootstrap a language standard library. It also removes responsibility from the language designer to actually design a good standard library and/or implementation. Anybody know why C++'s uninspired `unordered_map<T>` hash table can *still* be easily beaten on all metrics in \~100 lines of code?

# Why does a 50+ year old language consistently beat newer languages?

Let's investigate an interesting phenomenon. There's an infamous benchmarking site out there, [The Computer Language Benchmarks Game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/index.html). It contains small and contained programming "problems" like simulating particles with gravity. What's interesting is that, despite all the hype, Rust almost never comes first. C++ almost never comes first. It's usually C. And honestly? There's no good reason for this. Everything that C can do, C++ and Rust can do, and often better. So why does C keep coming up as the fastest language?

Here is why C is so fast: if you stick to the core libs but stay away from silly shit like `qsort()`, no superfluous code is being generated. Most of the *tiny* standard library is completely transparent to the compiler. This was attainable only because C is low-level: that is to say, dogshittingly useless barebones piece of crap. C only really has amenities for things that translate to small amounts of assembly, and makes you so painfully aware of every allocation and other higher level action that you rather seek out a way of doing things that doesn't need malloc or run time string manipulation or the other things you tend to get a lot of in higher level languages. It doesn't even have dynamic arrays for crying out loud. That's because there's a surprisingly high level of bookkeeping involved with them making them non trivial. Most times I find I don't actually need dynamic arrays. Statically sized, or at the least VLA (constant size but determined at runtime) really are enough most of the time, you just have to think about your code a bit.

Most of the time when C++ or Rust code beats a C solution it's due to the fact that it's easier to multithread in those languages, or they have more amenities for faster algorithms or approaches. And even with their overhead, those better approaches mean they still win. Again, *still* important to note they *still* don't often come out on top of C.

# High Throughput Fizzbuzz

But here's a controversial opinion: C is slow. Really slow. Embarassingly slow. Allow me to introduce you to this legendary post, [High throuhput Fizz Buzz](https://codegolf.stackexchange.com/questions/215216/high-throughput-fizz-buzz/). Scroll through it, the results speak for themselves.

As of today, the fastest entries are:

- Scripting langs: on the order of 0.1GiB/s
- Rust: 3.4GiB/s
- C++: 5.4GiB/s
- C: 20.9GiB/s
- Assembly: 57.2GiB/s

This is embarassing. It's a serious problem. I know that fizzbuzz isn't a good representative sample of most code running out there, but I find it deeply worrying that even performance-focused, clever code in the fastest language, C, leaves a roughly 2.7x speedup on the table over assembly. Even more worrying is that today's industry standard low level language, C++, leaves more than a fucking factor of ten. I'm honestly hoping that Rust's problem here is the fact that there's not enough Rust programmers out there that care about this to enter the competition with faster entries, but, well, it's looking even worse for Rust from this data. It got absolutely steamrolled by the 16.8 times faster assembly solution.

And why?

# Why Rust was slower than C

The pattern of fizzbuzz output repeats every 15 iterations. [Aiden's Rust solution](https://codegolf.stackexchange.com/a/217455/100457) makes use of this fact along with output buffering. It's a noble start.

[xiver77's C solution](https://codegolf.stackexchange.com/a/239848/100457) additionally breaks out pieces of inline assembly, manual vectorization, and scary looking hexadecimal magic numbers. It makes use of low level details like function calling convention. In some ways, it's more like assembly than C.

In my opinion, this is the sort of stuff that a compiler should be able to handle. If it weren't for xiver77's toil, it seems the C solution would be on par with the slower Rust and C++ solutions.

# Why C was slower than assembly

Here is [ais523's assembly solution](https://codegolf.stackexchange.com/a/236630/100457). On top of the previous techniques, the author had a clear goal: to stuff a certain amount of instructions in each cycle and a certain amount of data in the cache, which is clear, measurable and actionable. The C and Rust programmers weren't even thinking about this. It also steers clear of slow standard library implementations (called it). As a result, it is likely the fastest fizzbuzz implementation on the planet. But at a steep cost: "I've spent months working on this program".

# But why can't the compiler just think of all this?

Optimization is a balancing game. There's almost always the memory/runtime tradeoff: sure, you can build a giant hash table containing all solutions to a problem, which you can navigate in O(1) time. But how much memory does this cost? Today's compilers tend to not employ optimizations that would bloat memory usage. They also tend to be blind on the algorithm level: they can't "see" that kind of structure unless you hardcode support for it, so compilers just can't swap out a bad algorithm for a better one. And it *is* hard: imagine you are a dumb computer. How are you supposed to deduce all of modern mathematics and computer science on your own and realize that the quicksort can be swapped out for a radix sort when all you're working with is some sort of pseudo-assembly intermediate code representation? How do you even know what a quicksort or a radix sort is? How do you know what "sorted" is? Oh, that's clever, you looked up the mathematical definition of sorted, but my point is that that idea will simply never come up in a non-sentient computer. Right now, it can't even think of output buffering.

To make an optimizing compiler you have to teach it how to recognize patterns. It's up to the compiler engineer to hard code those patterns and what to do with them. To teach a computer how to learn and take automatic action on patterns? Well listen, I know computers can recognize images and play chess and stuff, but I don't want black box machine learned compilers compiling my code into god knows what, how would we even know if it's correct? And hell, we don't even understand most of this ourselves: most algorithms out there are pretty naïve and finding better ones is damn hard. Even the primordial art of sorting is a field where advances are still being made.

The easiest, smallest thing would I think be making the standard library, the core of the language, transparent to the compiler. It's been somewhat done in C and C++ and undoubtedly Rust, but so far no one seems to be truly devoted about it. No one even bothers to update the implementation of `unordered_map<T>`.
