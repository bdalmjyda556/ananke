Parser (worker):
If it encounters an symbol s of unknown kind, determine the set M of modules it can belong to. Query the oracle for KIND(s, M). Suspend.

If it encounters an exported symbol e of kind k belonging to module m, query the oracle for DEF(m, e, k). Do not suspend.

If it encounters module i to be included, query the oracle for PARSE(i). Do not suspend.

Oracle:
Keep table of modules=>symbtab and completed(bool).
For each module keep a table of symbols=>kinds.
Keep a table of requested symbols {status=resolved/unresolved, symbol, from_modules, parser}

PARSE(i)
    if i is not present in the table of modules, dispatch a parser for it.

DEF(m,e,k)
    if m is not present in the table of modules, create.
    register e in the symbol table of m.
    if e is present in the table of requests, and m is present in that request's set of from_modules:
        if status=resolved, error out with a name collision error. The parser/parse is discarded as it is likely erronous.
        if status=unresolved, set status to resolved and wake the parser up with the new definition.

KIND(s, M)
    go through the symbol tables for M
        if s is found once, wake up the parser
        if s is found multiple times, error out with name collision
        if s is not found, but all the parsers for all M have completed, the symbol is undefined, error accordingly
        if s is not found, but not all parsers for all M have completed, park the request in the requests table.
