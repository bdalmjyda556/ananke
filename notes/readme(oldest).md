# Nouns, symbols

Ananke makes a distinction between a variable (symbol) and what the variable "holds" (a noun).

A variable is a symbol for a noun.

A noun can exist without a corresponding variable -> anonymous noun.

A noun is a discrete unit of data made up of bits.

Nouns have fully qualified types.

Types are primitives, or derived from primitives via typedef, struct, union, enum.

Types have qualifiers which constrain them.

A fully qualified type = type + constraints.

Note that constraints can be an empty set.

FQ-type: fully qualified type

Operators act on whole nouns.

# Preprocessor

## Stack size

You MUST set the stack size for your program in bytes with `#stack x`.

	#stack 1048576 // 1MebiByte

The preprocessor may only reach a `stack` directive once. A file with

	#stack 1048576 // 1MebiByte
	#stack 1048576 // 1MebiByte

Will not compile.

**Note:** this practice is not necessary nor recommended for libraries.

## Including and linking

Including source files and/or directing the linker
`#include` will paste the files contents directly.

The following 4 directives can be chained with `#ifdef`, `#if`, etc to generate context-aware/portable compilation and linking options

	#include header.htl // paste header file contents
	#include source.ctl // paste source file contents
	#archive archive.a // link archive file (static linking)
	#object object.o // link shared object file (dyanimc linking)

## pragma once

Pragma once is found in a lot of C and C++ code to make sure that source or header files are only included once. In Ananke, this is achieved by adding a program name directive. The compiler will only include a single instance of a named module.

	#name once_module

## Macros

	#define findstring replacewith
	#macro dosomething(x, y, ...) {
		printf(...);
		return x+y;
	}

# keywords

`u8` `u16` `u24` `u32` `u64` `u80` `u128` `u256`
`i8` `i16` `i32` `i64` `i128` `i256`
`f16` `f24` `f32` `f64` `f80` `f128` `f256`
`const` `mut`
`owned` `shared` `uniq` `safe`
`auto` `stack` `heap` `register` `retain` `garbage` `extern`
`native` `leo` `beo` `leb` `beb` `lolb` `lobb` `bolb` `bobb`
`if` `elif` `else`
`match` `any`
`and` `or` `xor` `not` `true` `false`
`for` `in` `to` `with` `while` `do` `skip` `break`
`goto` `label` `comefrom` `jumpto`
`main` `return`
`operator` `lassoc` `rassoc`
`construct` `iterate` `destruct`
`struct` `union` `enum`
`namespace` `use` `as`
`typedef`

Reserved, not yet implemented:

`thread` `yield` `atomic` `semaphore` `mutex` `crit` `async`
`destroy`
`volatile` `inline` `padded`
`try` `catch` `throw`
`discard`

Keywords are reserved words that are tokenized as their own class of token. However, you might stupidly want to name your variable "u32" even though it is a keyword, or perhaps an external library written in another language uses a keyword. In that case, you can use the keyword escaper `\\`:

	u32 \\u32 = 0;
	i16 \\match = 666;
	*f32 \\operator;

# Comments

No code is generated from these, during syntax tree generation they are removed.

	// comment until newline

	//[
		multiline
		comment
	//]

When a compiler finds the case insensitive strings "fixme", "todo" or "hack" in a comment body, it may do nothing, or issue a notice, a warning, or a snarky remark.

# Operators

	structs, unions, enums
		.	fieldname resolver
	namespaces
		.	namespace resolver
	pointers
		$	address of (Where's the money Lebowski?)
		@	dereference (seek AT address)
		->	follow (dereference + fieldname resolve)
		.	offset // access address + x bytes
		[]	access // access address + x*sizeof(@pointer) bytes
	arrays
		[]	access // arr[x] 
		..	range // a..b from a to b, including a and b
		,+..	seqarith // continue sequence a,b+..c until end of array or index c as if arithmetic, c is optional
		,*..	seqgeom // continue sequence a,b*..c until end of array or index c as if geometric, c is optional
	bitwise 	
		===	bits equals
		|	bits or
		&	bits and
		~	bits not
		^	bits xor
		$$	hamming distance
		<<	left shift
		>>	right shift

		^<<	left shift, keep first bit
		^>>	right shift, keep first bit
		<<^	left shift, keep last bit
		>>^	right shift, keep last bit
		<<<	left shift with carry bit
		>>>	right shift with carry bit

		.<	left rotate
		.>	right rotate

		+<	left rotate with carry bit
		+>	right rotate with carry bit

		(suffix/prefix operators)
		~~	reverse noun
		!!	inverse noun
		$!	parity
		?#	popcount, count number of 1 bits
		!#	inverse popcount, count number of 0 bits
		^?	offset of first 1 bit
		^!	offset of first 0 bit
		?^	offset of last 1 bit
		!^	offset of last 0 bit
	arithmetic
		+	plus
		-	minus
		/	divide
		*	multiply
		**	power
		++	increment
		--	decrement
		%	remainder (-21 % 4 = -1)
		%%	modulo (euclidian division remainder) (-21 %% 4 = 3)
	compare
		>	greater
		<	less
		>=	less or equal
		<=	greater or equal
		==	equal
		\=	not equal
	logic		
		!	not
		&&	and
		||	or
		^^	xor
	assignment
		=	is
		<=>	swap
	conversion
		:	convert
	ternary
		?
		?!
		? :
		? : ,
	reserved for operator extension
		«
		»
		¢
		¥
		€
		£
		·
		×
		÷
		±
		¤
		³
		²
		¹
		º
		°
		‰
		¬
		¦
		½
		¼
		¾
		¡
		¿
		§
		¶
		©
		ª
		®
		™
		†
		‡

**Assignment operators**

		=	is
		!=	isnot
		+=	isplus
		-=	ismin
		*=	ismul
		/=	isdiv
		%=	isrem
		~=	isbnot
		^=	isbxor
		&=	isband
		|=	isbor
		.=	ismember isoffset
		->=	isfollow
		[]=	iselem
		<<=	islshift
		>>=	isrshift
		?!=	isif
		!?=	isifn

Biggest operators first, from left to right. `y+++++x` -> `y++ ++ +x` -> compilation fail

**Operator extension**

Operators can be _extended_:

	operator assoc (lhs) op (rhs) return {
		return ;
	}

	operator (i32 lhs) + (f32 rhs) f32 {}
	operator ++ (u8 rhs) u8 {}
	operator (u8 lhs) ++ u8 {}
	operator lassoc (u8 lhs) ** (u8 rhs) u8 {}

By default, all binary operators only operate on two nouns of the same fq-type. Any attempt to mix and match will result in compilation failure.

For instance, scaling an array:

	operator * ([]i32 lhs, i32 rhs) []i32 {
		for elem in lhs {
			elem *= rhs;
		}
		return lhs;
	}
	[]i32 arr = [1,2,3,4]*5; // [5,10,15,20,25]

Defining the `*` operator like this automatically defines its assignment operator `*=`.

**Note:** The `?` may not be extended.

**Note:** You may not define new operators. For instance, you can not create a 😂 operator.

**Note:** You may not extend the bitwise operators.

## Pre and post ops

Shifting left and right:

	a<<b mid: shift a left b times
	a<< post: shift a left once, return old value of a
	<<a pre: shift a left once, return new value of a

Incrementing and decrementing:

	x++ post: add 1 and return old value of x
	++x pre: add 1 and return new value of x

## boolean logic

keys: `true` `false`
ops: `!` `&&` `||` `^^`

There are no booleans in control, but nouns have a contingent property `true` or `false`, on which these operators operate.

A noun made up of only zeroes is considered `false`.

A noun, if not `false`, is considered `true`.

If a boolean expression resolves to return a new noun, the noun shall be equal to false=0 or true=1 as if it were an unsigned integer of the same endianness and size.

	i32 a = 1337;
	i32 b = 555;
	// a && b => native i32 1
	// a ^^ b => native i32 0

Boolean logic short circuits, allowing this:

	errorHasHappened && exit();

If errorHasHappened == false, exit() will not be evaluated (because a&&b with a=false will always be false, so no need to evaluate the right hand side).

## Ternaries

	cond ? iftrue;
	cond ? iftrue : iffalse;
	cond ? iftrue : iffalse , eitherway;

	cond !? iffalse;
	cond !? iffalse , eitherway;

	x!?a
	x!?a,c

if x evals to `true`, return x, otherwise return a. Always execute, but don't return, c.

### Conditional overwrite

	mydata ?!= x;
	mydata ?!= fancyOverwriter();

if mydata is true, overwrite with x, otherwise keep mydata.

	mydata !?= x;

if mydata is false (!true), overwrite with x, otherwise keep mydata.


# Other characters

	precedence	( )
	code block	{ }
	arrays		[ ]
	templates	< >
	delimiter	;

# Literals

**Strings and letters**

 - Character literal: enclosed in `''`
 - String literal: enclosed in `""`

Use the `\` escape character inside of a string or character literal to generate a code point.

**Note:** character literals can only be one byte. For multibyte unicode literals, use a string.

**Numbers**

 - Decimal point: either `,` or `.`
 - Sign: `-` or absence of `-`
 - Seperator: `_`

Integer literals:

	'-'?[0-9_]+

Decimal literals:

	'-'?[0-9_]*[\.,][0-9_]*

The decimal literal `.` evaluates to `0.0`.

# main

All Ananke programs begin execution in the special `main` block.

	main {
		return 0;
	}

You can capture argc, argv, envp

	main (i32 argc, **u8 argv, **u8 envp) {
		return 0;
	}

The `return 0` is implicit in main.

# Types:

Ananke offers the following primitives from which you can construct all other types:

	u8 u16 u24 u32 u64 u80 u128 u256 u512
	i8 i16     i32 i64     i128 i256 i512
	   f16 f24 f32 f64 f80 f128 f256

 - The `yX` types are of X bits.

 - uX: unsigned
 - iX: signed two's complement

 - The `fX` types are IEEE binary floating points of X bits. There is no `f8` or `f512`. Note that `f16` is not the same floating point type that most ML engines use. `f80` is the highest native floating point precision of most AMD64 CPUs.

 - To store a blob of binary data, use u8. To store strings, use u8 for UTF-8 or u16 for UTF-16.

**Note:** the type of a literal is deduced from the context.

In C code, you often see things like `float x = 19.f`. In Ananke, this is just

	f32 x = 19; // will produce floating point number 19.0

## Declaration and modification

You can declare a variable by

	`fq-type name;`

You can initialize it too:

	`fq-type name = initializer;`

Or you can modify variables, call functions, etc:

	`name = modification;`
	`name = functioncall();`

## Type Qualifiers
	
These say something about the noun, not the variable.

[default], alternative

**Mutability:**

	[mut], const

 - `mut` means the noun is mutable, i.e. its data can be modified
 - `const` means the noun is constant, i.e. its data can not be modified

**Storage class:**

	[auto], stack, register, heap, retain

 - `auto` lets the compiler choose
 - `stack` puts the variable in the stack frame
 - `register` puts the data in a register
 - `heap` puts the data on the heap (will **NOT** deallocate on scope exit)
 - `retain` puts the data in some place so that it is allocated once and stays allocated until program exit. The data will be kept through function calls. See example in functions chapter.

**Endianness:**
	
	[native], leo, beo, leb, beb, lolb, lobb, bolb, bobb

 - `leo` little endian octet order
 - `beo` big endian octet order
 - `leb` little endian bit order
 - `beb` big endian bit order

 - `lolb` little octet little bit
 - `lobb` little octet big bit
 - `bolb` big octet little bit
 - `bobb` big octet big bit

Network byte order 32-bit integer for instace: `bobb i32`

x86-64 processor native byte order is `lobb`

For instance, on an x86 processor:

	i32 num = 0xdeadbeef; // in memory [ef][be][ad][de]
	bobb i32 = 0xdeadbeef; // in memory [de][ad][be][ef]

**Note:** incompatible fq-types must be explicitly converted. `bobb i32 + lolb i32 = compilation error`

**Ownership:**

Additionally, nouns have a special ownership "qualifier" available, `own`. 

	own i32 ownedNumber = 32;
	own* i32 ownedPointer; // C++ unique_ptr

`own` Makes sure that you do not destroy or reassign a noun after you have passed on ownership, for instance, to a function that takes an `own`ed noun as an argument. Rust calls this "borrow checking" and acts as if it is revolutionary. When used in conjuction with pointers, it implies memory responsibility.

## Pointer types

`*T`

Pointers are a special type of noun, with the implication that its data translates to an adress of another noun of type `T`. Void pointers do not exist. Either interpret/convert your type or use a `u8*` (semantically meaning "pointer to byte") or use templates.

	* mypointer; // compilation fail
	i32* mypointer = ; // ok
	u8* unknownData; // u8 pointers are preferred to typeless pointers in Ananke

Pointers always follow the native endianness, so the endianness qualifiers are disqualified. All other type qualifiers are available. 

## Deriving types

You might want to store an ASCII string. ASCII goes into characters of 8 bits, so we use the `u8` primitive:

	typedef ascii u8
	typedef Cstring <'\0'>ascii // null terminated array

You can use a string literal to instantiate u8 arrays or u8 array pointers:

	Cstring greeting = "Hello, world!";
	// same as C's `char[] var = literal;`

	typedef literal <'\0'> const ascii;
	const * literal in_data_section = "Hello, world!";
	// same as C's `char* in_data_section = "Hello, world!";`

You can use `typedef` for trivial types, but you can also create `structs`, `unions` and `enums`.

	typedef utf8 []u8;

	struct organism {
		*utf8 name;
		enum sort {
			plant,
			animal,
		}
		union {
			f32 waterPerYear;
			u8 legCount; 
		}
	}

## Fully qualified types

A fully qualified type is the complete, compound type of a noun. This includes, possibly recursively:

	pointer_qualifiers pointer type_qualifiers type

For instance,

	i32 q1;

The fq-type **of the noun** symbolized by `q1` is: `i32`

	const retain lobb i32 uniq const * q2;

fq-type is `const retain lobb i32 uniq const *`

	i32[3][3][3]* q3;

fq-type is `[3][3][3]*`

## Casting (interpretation or conversion)

Ananke offers you to either interpret a bunch of data as if it were a certain type, or convert the data from type a to type b.

For instance, you might want to convert an int to a float. This does not preserve the exact underlying bits of data as the structure of both is different. But it does preserve the numerical value (where possible).

### Conversion

	i32 number = 1;
	f32 conversion = float:number; // conversion is now 1.00000

The conversion operator `:` takes the right hand side and converts the underlying data to match the type of left hand side.

### Interpretation

But you might also want to fill a float with the exact bit pattern of what's held in that i32 variable:

	f32 interpretation = f32 number;

Interpretation does not modify the underlying bits. It just changes the associated type. It is especially handy when parsing data packets or binary blobs.

### Summary

**Conversion (`:`)** lets you modify the type while preserving meaning

**Interpretation** lets you modify the type while preserving raw data

# Arrays

Arrays are handled as a noun.

Wether a noun is an array or not, depends on its type.

## different kinds

There are 3 kinds of arrays: terminated arrays, statically sized arrays, and dynamically sized arrays. You can convert between these types as expected.

## Terminated Array

**A terminated array** goes on until a terminator element is reached. It can be put in the data section of a program or in heap memory or in a stack frame. In memory, it might look like this:
	
	[elem] [elem] [elem] [elem] [terminator]

Note that the terminator is of the same type as the elements.

You may already know this array type from C's `char*`.

	<Terminator>Type var;

## Statically sized array

**A statically sized array** has a compile-time known amount of elements. It can be put in the data section of a program or in heap memory or in a stack frame. In memory it looks like this:

	[elem] [elem] [elem] [elem]

## Dynamically sized array

**A dynamically sized array** has a dynamic amount of elements. It can only be put on the heap or in a stack frame. A 4 element dynamic array looks like this in memory:

	[4] [elem] [elem] [elem] [elem]

Meaning, a 32-bit unsigned integer describing how many elements there are, then the actual elements at offset=4 bytes.

If you want an array with more than 4.29 billion elements, you can specify the counter to be 64 bits:

	u64[] i32 arr;

To specify the endian-ness of this number, supply the arraytype itself a qualifier:
	
	lobb[] i32 arr1;
	// the array's count is lobb, the elements are native endian
	lobb[] lobb i32 arr2;
	// both the array's count and the elements are lobb

This approach also works for specifying a terminator's endianness.

**Note:** As you might expect, specifying the endian-ness of an array itself, when the array is static, does nothing.

## Notes

**Note:** All arrays are allocated, wherever they are allocated, as a continuous block, because they are a noun.

**Note:** The address-of operator, `$`, when used on an array, shall return the address of the first byte of a noun. This means that it will not necesarily return the address of the first element, such is the case with dynamic arrays, where it returns the address of the element count.

**Note:** Due to arrays being nouns, using a bitwise operator on an terminated array or dynamic array will also affect the terminator element or the count element respectively.

**Note:** To convert a dynamically sized array to follow the same format as a statically sized array (i.e., drop the u64 prefix), you can convert like so: `[0]type:array`. Keep in mind you must manually allocate memory and keep count then.

## Declaring arrays

 - Declaration of form...   Evaluates to...
 - `[0] type x = literal`    statically  sized array
 - `[]  type x = variable`   dynamically sized array
 - `[0] type x = variable`   FAIL
 - `[literal]  type x = any` statically  sized array
 - `[variable] type x = any` dynamically sized array

## Arrays as function arguments

Due to polymorphism, the exactly type-matching function is executed. 

	// this arrayfunc takes a dynamic array of integers
	fn arrayfunc([]i32 arr){
		printf("Dynamic version");
	}
	
	// this arrayfunc takes a static array of 8 integers
	fn arrayfunc([8]i32 arr){
		printf("Static version");
	}

	arrayfunc([0]i32[1,2,3,4,5,6,7,8]); // static version
	arrayfunc([]i32[1,2,3,4,5,6,7,8]); // dynamic version

	arrayfunc([]i32[1,2,3,4,5,6,7,8,9]);

Will fail because there is no function `arrayfunc` defined that accepts type `[9]i32`.

## Arrays and pointers

**Note:** In C, an array is not a pointer, despite sometimes similar usage syntax. The same holds true for Ananke.

Pointer to array or array of pointers?

	// array of (pointer to (int))
	i32 var1, var2;
	[]*i32 ptrarr = [$var1, $var2];

	// pointer to (array of (int))
	[]i32 = [var1, var2];
	*[]i32 arrptr = $arr; 

## Multidimensional arrays

	[3][3]i32 arr_2d = [[1,2,3],[4,5,6],[7,8,9]];
	// arr_2d is an (array of 3 (arrays of 3 (ints)))

And you can access nested items with the `.` access operator:

	arr_2d[2][1] == 8; // will evaluate true

Assinging arrays **copies values** rather than a 'pointer to array' like in C.

	[3]i32 subarr = [1337,420,69];
	arr_2d[1] = subarr;
	subarr[2] = 2; // previously 69
	arr_2d[1][2] == 2; // will evaluate false, because arr_2d.1.2 is still 69

To get the length of an array, you can use sizeof(arr)/sizeof(arr[0])
sizeof is compile time, so sometimes it will not work. You must then manually track length.

	printf("test has %d elements!",sizeof(test)/sizeof(test[0]));
	printf("test takes up %d bytes of memory in total!",sizeof(test));

## ranges, sequences and initialization

The range operator `a..b` takes 2 numbers a and b and turns it into an array from a to b.

	[0]u8 arr = 1..5;
	// arr = static [1,2,3,4,5]

	[]u8 arr = 1..5;
	// arr = dynamic [1,2,3,4,5]
	<0>u8 arr = 1..5;
	// arr = <1,2,3,4,5><0>

When we initialize an array with a pattern, we can continue the pattern with `...`, `..+`, `..*`, The compiler can detect (fractional) addition and (fractional) multiplication.

	[5]i32 arr = [1,2..+];
	// arr = [1,2,3,4,5]

	[5]i32 arr = [1,2...];
	// arr = [1,2,3,3,3]

	[5]i32 arr = [1,2..*];
	// arr = [1,2,4,8,16]

Because the compiler only looks at the last 2 elements. All of these methods return an array of whatever number type the first range operand is. The array is dynamic by default, but a static array can be created by putting these generators as an initializer of a static array.

## methods

Arrays have a _couple_ of built in methods. Remember, if you declare your strings as arrays of characters, you get these methods "for free".

**Note:** if you need to string together multiple methods, it might be best to write a custom algorithm.

## size, elems

`array.size()` return the size of the array (including terminator or count) in **bytes**.

`array.elems()` returns the amount of elements in the array.

 - For static arrays this gets compiled away
 - For dynamic arrays it is a O(1) lookup to the element count
 - For terminated arrays it is a O(n) lookup to count until the terminator is hit

## contains, overlaps

`haystack.contains(needle)` returns `true` (u8 0x01) if all elements in `needle` can be found anywhere in `haystack`, or `false` (u8 0x00) if not.

`haystrack.overlaps(needle)` returns `true` (u8 0x01) if at least 1 element of needle can be found in `haystack`, or `false` (u8 0x00) if not.

## find, seek

Unlike `contains`, these search functions look for exact matches.

`haystack.seek(needle)` returns a dynamic array of 1 or 0 indices. 1 index at which `needle` can be found in `haystack` or 0 indices if `needle` can not be found. 

Couple `.seek` with `.elems` to get a true/false exact match result.

`haystack.find(needle)` returns a dynamic array of indices at which `needles` can be found. Empty array if `needle` can not be found in `haystack`.

## hamming, hammask

`array.hamming(array2)` returns the hamming distance between the arrays as a number.

`array.hammask(array2)` creates a new array where elements i are `true` if array[i] != array2[i], `false` otherwise.

## concat, union, intersect

`array.concat(array2)` returns 2 arrays concatenated back-to-back.

`array.union(array2)` returns a new array with all elements of `array`, and all not-already-present elements of `array2`. Otherwise back-to-back.

`array.intersect(array2)` returns a new array with all elements that exist in both `array` and `array2`. May be an empty array.

## deduplicate

`array.dedup()` returns a new array without duplications.

## sorting

`array.quicksort(comp)`

`array.mergesort(comp)`

`array.radixsort(comp)`

Where comp is a 2-callable (function, lamda, operator that takes 2 arguments) to sort by. If comp is not provided, it will be sorted in ascending order.

Comp MUST be provided if the array is not of primitives.

## pluck, extract

`haystack.pluck(needle)` returns a new array, haystack without any element that exists in needle.

`haystack.extract(needle)` returns haystack without any exact substring/subarray match of needle.

## reverse

`array.reverse()` reverses an array.

## insert, remove, add, push, shift, pop

`array.insert(elem, index)` inserts an element to a specific index, and shifts the rest of the array further back to accomodate it.

`array.remove(index)` removes a specific element from an array by index and shifts the rest of the array forward to fill up the gap.

`array.push(elem)` = insert(elem 0)

`array.shift()` = remove(0)

`array.shift(n)` removes first n elements

`array.add(elem)` = insert(array.elems())

`array.pop()` = remove(array.elems())

`array.pop(n)` removes last n elements

## map, zip, filter, reduce

**callable**: operator, function (lambda, method, literal function, etc)

 - 1-callable: callable that takes 1 input/arguments
 - 2-callable: callable that takes 2 inputs/arguments

### map

`map` will apply a 1-callable to each element in the array.

It maps a function to an array.

	`[]i32 arr = (1..5).map(++);`
	// arr is [2,3,4,5,6]

map is equivalent to
	
	fn map(arr, callable){
		for elem in arr {
			elem = callable(elem);
		}
		return arr;
	}

### zip

`arr.zip(arr2, func)` will, given 2 same type and same length arrays `arr` and `arr2`, return a single array where each element is constructed by applying the 2-callable `func` to each pair of elements from the two arrays.

### filter

`filter` will, for each element of an array, keep or discard it based on a 1-callable applied to that element.

It filters the array.

	fn even(i32 x){
		return x%2 ? 0 : 1;
	}
	arr = arr.filter(even);
	// arr will be [2, 4, 6]

filter is equivalent to

	fn filter(arr, callable){
		filtered;
		for elem in arr{
			if callable(elem){
				filtered.add(elem);
			}
		}
		return filtered;
	}

### reduce

`reduce` will pass each element of an array to a 2-callable.

It reduces the array to a single thing.

	i32 sum = arr.reduce(+);
	// sum = 2+4+6 = 12

	arr.reduce(print) -> (print(print(2, 4)), 6) -> FAIL

As you can see, reduce produces recursion. `+`, which is `ADD(a,b)` can handle this but not `printf`.

### chaining

We can chain all these expressions into one:

	i32 sum = (1..4).add(5).map(++).filter(even).reduce(+);

A good compiler will make this efficient, i.e. consolidate all the array returns. Armed with a bad compiler, you might be better off writing this sort of stuff manually.

# Control flow

## goto

`goto` `label`

goto can be handy for breaking out of many layers of loops. Otherwise `do` gives the same functionality as `goto loop`.

	label loop;
	goto loop;

## jumps

`jumpto` `comefrom` `checkpoint` `with`

In a sense, jumps feel similar to functions. However, they are not. Instead of allocating a new stackframe, they jump back to the stackframe of "comefrom". It allows for intricate handling of errors. If you have ever used `setjmp` in C, this is that, but prettier.

	checkpoint error i32;
	// special declaration
	// error "stores" the stackframe and cpu stack-related registers
	// the return type is i32

	fn foo {
		jumpto error with 4;
	}

	fn main {
		foo();

		comefrom error with code {
			match code {
				4 {
					printf("Recoverable error 4");
				}
				any {
					printf("Unusual error");
					exit(code);
				}
			}
		}
	}
	

## for loops

`for` `in` `to` `by`

For loops are used to count and or iterate.

	for i32 i=0; i<10; i++ {
		printf("%d", i);
	}

Is functionally equivalent to (`to` is tail-exclusive, i.e. 10 will never be reached)

	for i32 i = 0 to 10 {
		printf("%d", i);
	}

In increment of 2:

	for i32 i = 0 to 10 by 2 {
		printf("%d", i);
	}

Equivalent notation:

	for i32 i = 0, 10, 2 {
		printf("%d", i);
	}

You can easily iterate over arrays, or other structures with `implicit iterators`:

	i32[10] arr = [0..9];
	for elem in arr {
		println(string:elem);
	}

`elem` will in this casse be of type `i32`, the underlying type of the array.

With an index:

	for i32 i, elem in arr {
		println(string:i +" is "+ string:elem);
	}

	for k, v in table {
		println(string:k +" is "+ string:v);
	}

## while do and do while, break, skip

	while condition {
		skip; // equivalent to misnomer "continue" in most languages
	}
	
	// code first, condition second
	do {
		// code
		if exitcond {
			break;
		}
	}

	// is equivalent to
	
	do {
		// code
	} while !exitcond;

	do 12 {
		// repeats exactly 12 times, then checks condition, then another 12 times or break, etc
	} while !exitcond
	
	// condition first, code second
	do {
		if exitcond {
			break;
		}
		// code
	}

	// is equivalent to

	while !exitcond {
		// code
	}

## switch case

	match fall any

	match var {
		2 fall; // fall through to next item
		3 {
			// do some prepocessing
			fall;
		}
		othervar {
			// final
		}
		any { // var matches none of the above
			break; // break out of the nearest loop
		}
	}

Pattern matching is supported

	match v1, v1 {
		1,0 statement;
		1,1 statement;
		0,any statement;
	}

## if/elif/else

	if cond {

	} elif othercond {

	} else {
		
	}


# functions

`return`

Define a function

	i32 func(args) { return 1; }
	i32(args) func2 = { return 1; };

Storing functions as variables

	i32(args_prototype) func3 = func;

Anonymous functions
	
	i32 var = [capture]{ return 1; };

Closures

	i32(args) closure = [capture]{ return 1; };

Templated functions

	i32 functiontemplate<temp>(args){}
	i32(args)<temp> functiontemplate = {};

	i32() myfunc = functiontemplate<x>

Templated closures

	i32(args)<temp> closuretemplate = [capture]{};

**Note:** as you can see, depending on how you declare or assign a function you might not need a semicolon.

Define a function that returns nothing

	print(i32 n) {
		printf("%d", n);
	}
	print(7);

Function with retain variable

	myfunc(<utf8 '\0'> string) {
		retain utf8 seperator='';
		// static: variable value will be "kept" between function calls
		// (it will be assigned into the BBS section of the executable rather than on a stackframe)
		printf("%c %s \n", seperator, string);
		seperator=',';
		// first run, seperator is ''
		// all next times, seperator is ','
	}

## multiple return values

	i32, i32 myfunc(args) { return x,y; }
	i32 a,b = myfunc();

## polymorphic functions

You may define a function multiple times for handling different types. The compiler will automatically resolve the correct function to call from the argument type.

	use <u8 '\0'> as c_string;
	use []u8 as utf8;
	
	print(c_string str){
		printf("Print was called on a ISO C string");
	}
	print(utf8 str){
		printf("Print was called on a Ananke string");
	}
	print {
		printf("Print was called without any arguments - huh?");	
	}

	print(c_string"What's up? 1972 here!");
	print(utf8"pop pop skrrrraaa");
	print();

If no type match is found the compilation will just fail.

	print(4.65); // fail

**Note:** function variables can not be polymorphic. They must have an argument prototype.

## Variadic functions

A function can take an arbitrary amount of arguments with a `...x` argument. (that is, until a too-massive amount of arguments overflows the stack).

The function can access the varargs as a `u64[] u8`, a dynamic array of u8 with a 64-bit elem-count.

It is up to the programmer to extract and interpret the actual values and interpret their types in this array.

	printf(string format, ...varargs)
	{
		string buffer;

		i32 i = 0;
		i32 vari = 0;
		while format[i] \= '\0' {
			if format[i] == '%' {
				i++;
				match format[i]
				{
					'%' {
						buffer += '%';
					}
					'c' {
						buffer += varargs[vari++];
					}
					's' {
						string* tmp = *string $varargs[vari];
						vari += tmp->size();
						buffer += @tmp;
					}
					'S' {
						cstring* tmp = *cstring $varargs[vari];
						vari += tmp->size();
						buffer += @tmp;
					}
					'f' {
						f32* tmp = *f32 $varargs[vari];
						vari += 4;
						buffer += string:@tmp; // conversions truly make things too easy
					}
					any x {
						buffer += "format '%" + x + "' is not supported!";
					}
				}
			}
		}
	}

Of course, rather than writing our own `printf` it's easier to just `print("head " + string:number + " tail");`.

## lambdas

You can create a lamda, or inline anonymous functions, for parameters that require a function.

	newarr = array.map(i32(i32 x){return x+1;});
	i32(i32 x) mylam = {return x+1;};
	newarr = array.map(mylam);

# Custom operators

`operator` `lassoc` `rassoc`

you can add operators for your structs:

	struct vec3{
		f32 x;
		f32 y;
		f32 z;

		constructor(f32 x, f32 y, f32 z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
	
	// left associativity (left is evalled first)
	operator lassoc + (vec3 lhs, vec3 rhs) vec3 {
		return 	(lhs.x+rhs.x, lhs.y+rhs.y, lhs.z+rhs.z);
	}

	vec3 result = vec3(1,2,3)+vec3(4,5,6);

You may not overload type-agnositc operators, such as bitwise operators or assignment. They will ALWAYS act on the raw noun's bits.
For this, methods.

# Pointers

`*` `@` `$` `owned` `shared` `uniq` `safe`

safe pointers are checked to not be null at dereference

uniq pointers are guaranteed to be the only pointer to that address
	
owned pointers imply memory ownership.

shared pointers are reference counted fat pointers.

	*i32 p; 
	// defines p to point to an int
	// currently, p points nowhere
	p == null; 
	// known as NULL in C and nullptr in C++
	i32 a = @p;
	// dereference p / indirection
	// read as "a is at p"
	i32 x = 25;
	uniq* i32 y = $x;
	// defines y as a unique pointer, holds address of ($) x

Function pointers

	fn simple() {return;}
	* () fnptr = simple;

	fn func(u8 x, i32 y) f16 {return 0;}
	* f16(u8, i32) funcptr = func;

# Linked list example

	namespace ll {
		// template
		struct node <T> {
			T data;
			*node next;

			iterator(*node<T> start) T {
				retain *node<T> curr = start;
				
				if curr != null {
					T retval = curr.data;
					curr = curr.next;
					return retval;
				} else {
					break;
				}
			}

			constructor([]T arr) node<T> {
				*node<T> current = $this;

				for elem in arr {
					*node<T> next = *node malloc(sizeof(node<T>)) or error("Could not allocate node!");
					current->next = next;
					current->data = elem;
				}

				free(current->next);
				current->next = null;

				return this;
				// the return statement is reduntant.
			}
		}
	}

	[]i32 arr = [0,1,2,3,4,5,6,7,8,9];
	ll::node<i32> list = ll::node<i32>(arr);
	for data, i in $list {
		printf("Node %d holds %d", i, data);
	}

	// For loops of this form call the iterator method
	// Construction of a struct of the form struct() calls the constructor method
