# Continuations: Pointers for Control Flow on Steroids

Special thanks to Wreien.

Continuations are one of the most interesting and powerful concepts I have come across in programming. They can be used to implement literally any type of control flow, from a `return`, to a coroutine's `yield`, to a loop, even exception `throw`ing and `catch`ing. What a pointer is for data, a continuation is for execution. Like Ying and Yang, Datastructure and Algorithm.

Ahem. So to understand a continuation, we must first go back to basics, much like how you can not understand pointers without understanding what a piece of data is. We revisit the humble `goto`:

```c
label:
puts("Hello!");
goto label;
```
This poor program loops infinitely, chanting its greeting in vain. Of course, a loop can be implemented with a `goto`, which was par for the course in old, nay, ancient languages like early ALGOL:
```c
for (int i = 0; i<10; i++){
	printf("i = %d", i);
}
```
```c
int i = 0;
loop:
if (i<10){
	printf("i = %d", i);
	i++;
	goto loop;
}
```
This `goto` stuff seems pretty useful... But what if we could store a label in a value? We could change the value, and dynamically `goto` different points! COBOL _sort of_ had an _absolutely revered_ feature like that called `ALTER`, and FORTRAN has a feature called "computed goto": you dump an array of line numbers into your `goto`, and you then specify _which one_ to jump to dynamically. But it's not storing a label as a variable.

Well, storing a label, what does that mean? In fact what does it mean to `goto`? It's really just a `jmp` assembly instruction. Which, inside the CPU, changes the "program counter", which is a special register which tells the CPU the memory address of the next instruction to be executed. So a label is really just a special variable that holds an address of some instruction from which the CPU should continue execution. And there you have the word: continuation.

Now, before I alienate and infuriate my functional programming audience, I should quickly mention that, while yes, continuations are the goto of functional programming, continuations often come with more perks than just a low level goto:

1. A goto can only jump to a visible label according to scoping rules. A continuation can lead anywhere.
2. A continuation usually carries a "return value".

Return value? Let's see... When you return from a function call, what are you doing? You transfer control to back where you came from. Kind of like a `goto`! But you also send a piece of data back.

```c
int add4(int x){
	return x + 4;
}

int main(){
	int retval = add4(4);
}
```
So far so good, but let's see what this looks like when implemented with those _fundamental_ continuations. The idea is we make a snapshot of the current execution state (A.K.A. current continuation) of the program. We then **invoke** the continuation - thus re-entering that snapshot - when we are done.

In C - little known fact: C has continuations! - this snapshot is packaged up nicely with a `jmp_buf`.
```c
#include <setjmp.h>
#include <stdio.h>

void add4(int x, jmp_buf ret){
	longjmp(ret, x+4);
}

int main(){
	jmp_buf curr_cont;
	int retval = setjmp(curr_cont);
	if (retval == 0){
		// we arrive here when we first set up our continuation
		// in that case, setjmp returns 0
		add4(4, curr_cont);
	} else {
		// we arrive here after add4
		// in that case, setjmp returns non-0 (the return value)
		printf("4+4=%d\n", retval);
	}
}
```
This, in essence, is how a continuation works. You might have a question like "what if I _actually wanted_ to pass the value of 0?" to which C replies: "Sorry, I'm too busy overflowing my buffers to hear you!"

So anyway, earlier I made some crazy claims like "exception handling can be implemented with continuations" - YES! And a hell of a lot more efficiently than the currently used stack-unwinding exception handling algorithms!

Let's say we want to "throw" an "exception" when the provided x is larger than 3 or smaller than 0 (our computer isn't worthy of computing those cases, and it also solves the problem of not being able to return the value 0):
```c
#include <setjmp.h>
#include <stdio.h>

enum error_codes{
	e_none=0, //setjmp reserved the number 0, remember :(
	e_too_large,
	e_too_small
};

const char *errors[] = {
	[e_too_large] = "Number too large!",
	[e_too_small] = "Number too small!"
};

int add4(int x, jmp_buf exception){
	if (x > 3){
		longjmp(exception, e_too_large);
	}
	else if (x < 0){
		longjmp(exception, e_too_small);
	}

	return x+4;
}

int main(){
	jmp_buf curr_cont;
	int error = setjmp(curr_cont);
	if (error == 0){
		// we arrive here when we first set up our continuation
		int retval = add4(4, curr_cont);
		printf("4+4=%d\n", retval);
	} else {
		// we arrive here from the continuation
		// but we only invoked the continuation on erronous conditions
		// therefore this is the exception handler
		printf("Error occured: %s", errors[error]);
	}
}
```
Sure, it looks a bit rough around the edges. But if you squint really hard, you'll see the contours of a `throw` inside `add4`, and a `try/catch` block inside `main`.

But it is kind of clunky, I admit. `setjmp` can only return an `int` so we have to do all this string-lookup bookkeeping. But we can reduce this clunk, and simultaneously make our intent clearer, if we explicitly handle both exceptions with a seperate continuation. Let's see it!

```c
#include <setjmp.h>
#include <stdio.h>

int add4(int x, jmp_buf too_small, jmp_buf too_large){
	if (x < 0){
		longjmp(too_small, 1);
	}
	else if (x > 3){
		longjmp(too_large, 1);
	}
	
	return x+4;
}

int main(){
	jmp_buf too_small;
	if (setjmp(too_small)){
		puts("Error: number too small!");
	}

	jmp_buf too_large;
	if (setjmp(too_large)){
		puts("Error: number too large!");
	}
	
	int retval = add4(4, too_small, too_large);
	printf("4+4=%d\n", retval);
}
```

And now `add4` doesn't need to worry about pesky error handling or string bookkeeping. It just _continues_ the program execution to the appropriate place. Also note that, as the program is currently structured, the continuations form a loop! It will loop and complain, until the user figures out a number which is in range; good luck, user!

Note that it's all `goto`, or rather `jmp`-instructions under the hood. And this exception mechanism doesn't unwind the stack, looking for a handler at each frame. It jumps _directly_ to the handler it _knows_ exists.

Now, let's go all out. Let's even throw in `scanf` input handling - nasty stuff.

```c
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>

void add4(int x, jmp_buf too_small, jmp_buf too_large, jmp_buf return_ok){
	if (x < 0){
		longjmp(too_small, 1);
	}
	else if (x > 3){
		longjmp(too_large, 1);
	}
	else {
		longjmp(return_ok, x+4);
	}
}

int main(){
	jmp_buf too_small;
	if (setjmp(too_small)){
		puts("Error: number too small!");
	}

	jmp_buf too_large;
	if (setjmp(too_large)){
		puts("Error: number too large!");
	}

	jmp_buf not_a_number;
	if (setjmp(not_a_number)){
		puts("Error: not a number!");
	}
	
	puts("enter a number:");
	int input;
	// scan a decimal integer
	switch(scanf("%d", &input)){
		case 0:
		// was not a decimal integer
		// flush input and try again
		scanf("%*[^\n]");
		longjmp(not_a_number, 1);

		case EOF:
		// no more input, ever
		puts("...alright then");
		exit(1);
	}
	
	jmp_buf return_ok;
	int retval = setjmp(return_ok);
	if (retval){
		printf("%d+4=%d\n", input, retval);
	} else {
		add4(input, too_small, too_large, return_ok);
	}
}
```

Ok, the `return_ok` return continuation was a bit excessive. Maybe. At least it's a consistent codebase. But the rest clearly holds!

Pros:
* very readable once you get used to the `setjmp`/`longjmp` idiom
  * the peculiarity of `scanf` is however out of my control
* we know exactly which exceptions `add4` can throw
* all of the exceptions are forced to be handled
  * if you want one catch-all, you can do so by passing the same jumpbuf as multiple args
* exceptions are handled at the top, which allows us to effortlessly create retry-loops
* the happy case is handled below that
* the actual dirty work, `add4` is only called at the bottom

One last pro: continuations allow for effortless refactoring. We can easily lift the `scanf` switch case out into a new function that takes the continuation - ahem, `jmp_buf` - to the "not a number" handler. This time I won't use a return continuation, I promise.

```c
int read_number(jmp_buf not_a_number, jmp_buf end_of_file){
	int input;
	switch(scanf("%d", &input)){
		case 0:
		// flushing input is now the responsiblity of handler
		longjmp(not_a_number, 1);

		case EOF:
		// exiting is now the responsibility of handler
		longjmp(end_of_file, 1);

		default:
		return input;
	}
}
```
As you can see, continuations are very powerful. Or rather, they are _fundamental_. However, continuations in C have some limitations, due to C only having a single call stack. This is illegal for instance:
```c
#include <setjmp.h>
#include <stdio.h>

jmp_buf continuation;

void fizz() {
	int result = setjmp(continuation);
	if (result == 0) {
		puts("Set up the continuation");
	} else {
		printf("Got %d\n", result);
	}
}

void foo() {
	fizz();
	puts("we're in foo!");
}

void bar() {
	longjmp(continuation, 5);
}

int main() {
	foo();
	bar();
}
```
First, `foo` is called. `foo` calls `fizz`. `fizz` sets up a continuation and exits, back to `foo`. `foo` exits, back to main. `bar` is called and jumps to the _just_ destroyed stackframe of `fizz` (you witnessed it! we returned from `fizz`, thus destroying its stackframe!). From there, we will return "back" to the also destroyed stackframe of `foo`, from which we will return back to `main`'s stackframe, but we return to the point in `main` just before `bar` is called. So we call `bar`, and we enter the loop again.

Now, you can't safely jump into a destroyed stackframe. This is deeply undefined-behaviour territory. That said, this example works (looping infinitely as expected) in x86-64 gcc v11.2: https://godbolt.org/z/q34h1WjfM

This is an issue of a single call stack language like C. In a language like Scheme, which uses the slightly differently wielded `(call/cc)` instead of `setjmp()`, this is not an issue: Scheme has stackful continuations. Each continuation keeps a copy of the entire call stack. Or something like that - I'm actually not privy on the _exact_ implementation details of Scheme - I just know it does more bookkeeping than C.
