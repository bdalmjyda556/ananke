easy pickings:
two print calls can be made into a single print call of the concatenated string. If the compiler can make the concatenation at compile time, it serves it well to make this optimization. From everything I've heard, syscalls are to be avoided at all costs, and imperative evidence suggests output buffering is always good. In fact most forms of output probably require synchronization to even more reason to bundle up so we don't waste all of our time waiting for mutexes.

theseuic functions: from the ship of Theseus. If you have two ships made of the same blueprint, they are maybe not the same, but they _are_ equivalent.

example:
```
con = connect("db1")
con.query("insert values :x", [1,2,3])
con.close()
con = connect("db1")
res = con.query("select *")
con.close()
```
is actually the same as
```
con = connect("db1")
con.query("insert values :x", [1,2,3])
res = con.query("select *")
con.close()
```
which is _some cases_, namely assuming we are the only one mutating the database and we started from an empty table, is the same as
```
res = [1,2,3]
con = connect("db1")
con.query("insert values :x", res)
con.close
```

because `connect()` and `close()` form a theseuic pair. Despite `con` being a different object, it is an indirection of the same object, namely the same database (`db1`).

In that same sense, `malloc()` and `free()` also form a theseuic pair, meaning that a `free()` followed by a theseuic `malloc()` (so the region's sizes are at least equal) the `free()` and `malloc()` can be elided and the buffer can just be reused.

For file I/O, create and delete are also such a pair. Though we have file_open, which is more ambiguous. Does the file already exist?

```
handle = file_open("tmp.txt")
file_write(handle, "lol!")
string x = file_read(handle)
print(x)
file_delete("tmp.txt")
```
_could_ become
```
print("lol!")
if (file_exists("tmp.txt")){
    file_delete("tmp.txt")
}
```
But this code is very race-y: it's full of race conditions like time of check until time of use with the `if exists` and `delete`, as well as assuming that the file_write and file_read are one atomic operation (they are not, someone else might have written `lol!2` to it in between). It also assumes no one is watching for the creation of a file called `tmp.txt`, and it also assumes no on is watching for the contents of a file called `tmp.txt` to change. These are some pretty lofty assumptions that a compiler really can't make on its own: it needs extra programmer-specified context to figure this out.