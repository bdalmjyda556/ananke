# Replacing inheritance with sum types

Consider the following C++ OOP code: you have entities with a bunch of stats, and different types of entities with slight variations on the values. It uses inheritence, _obviously_.

```cpp
#include <iostream>
#include <memory>

struct Stats
{
    int _hp = 0;
    int _mp = 0;
    int _strength = 0;
    int _speed = 0;
    int _intelligence = 0;
};

class IEthnicity
{
public:
    virtual ~IEthnicity() = default;
    virtual std::string getEthnicity() const = 0;
    const Stats &getStatus() const { return _stats; }

protected:
    Stats _stats;
};

class Human : public IEthnicity
{
public:
    Human()
    {
        _stats._hp = 5;
        _stats._mp = 5;
        _stats._strength = 5;
        _stats._speed = 5;
        _stats._intelligence = 5;
    }

    std::string getEthnicity() const override { return "Human"; };
};

class IClass
{
public:
    virtual ~IClass() = default;
    virtual std::string getClass() const = 0;
    const Stats &getStatus() const { return _stats; }

protected:
    Stats _stats;
};

class Mage : public IClass
{
public:
    Human()
    {
        _stats._hp = 0;
        _stats._mp = 10;
        _stats._strength = 1;
        _stats._speed = 3;
        _stats._intelligence = 11;
    }

    std::string getClass() const override { return "Mage"; };
};

class Player
{
public:
    class Builder;

private:
    Stats _stats;
    std::string _name;
    std::unique_ptr<IClass> _class;
    std::unique_ptr<IEthnicity> _ethnicity;
};

class Player::Builder
{
    // Choose the Ethnicity and Class
};

class IFactory
{
public:
    virtual ~IFactory() = default;
};

int main()
{
    Human A;
    Player player;
    // build player here
    std::cout << A.getStatus()._hp << std::endl;

    return 0;
}
```

```ank
; product type: ,
type stats {
  var hit_points,
  var magic_points,
  var strength,
  var speed,
  var intelligence
}

; sum type: /
type entity {
  stats::{0,10,1,3,11} mage /
  stats::{5,5,5,5,5} human /
  stats::{999,999,999,999,999} Ananke
}

fn attack(mage m){
	// the attack method for mages
}

fn attack(human h){
	// the attack method for humans
}

fn attack(Ananke a){
	// the attack method for Ananke
}
```