These functions return a modified copy of array. In case of a `var` array, the compiler is smart enough to mutate in place.

function.
`sample(n)`: make an array by sampling a function. Equivalent to `[function].rep(n).map(\f f())`

array.
`len()`: how many items the array has
`cap()`: how many items the array can fit

`ins(el)`: append el 
`ins(n,el)`: arr[n] = el, but the rest of the array is pushed back to make space, no overwrite
`del(n=1)`: remove last n elems
`rep(n)`: grow the array by repeating the elements n times

`push(el)`: prepend el
`pop(n=1)`: remove first n elems

`extract(n)`: remove arr[n] and shift arr[n+1] to arr[n] etc to fill the gap
`sad(el)`: [Search And Destroy] remove all el (by value)
`slash(n)`: [Swap Last And SHift] swaps arr[n] with arr[len()], then shifts the array to remove it.

`set(n,el)`: arr[n] = el
`get(idx)`: where idx is an array of indices, returns the subarray of arr[idx[0]]..arr[idx[n]]

`shuffle()`: shuffles an array
`sort()`: returns sorted array
`sort(f)`: return sorted array with user supplied dyadic comparator function f
`[q,r,m,i]sort(f)`: quick-, radix, merge, insertion sort, also optional comparator parameter

`tally()`: tally([1,1,1,9]) == [1:3, 9:1]
`keys()`: gives an array of all keys of the array/table

`roll(n)`: gives an array of each n-wide window into the array. `data.roll(5).map(average)` makes a moving average with a window of 5.
`shuffle()`: returns shuffled array (fisher yates)
`swap(i0,i1)`: returns array with swapped elements i0 and i1
`rot(n,a=1)`: rotates array/matrix around axis a by n
`zip(arr2)`: zip(1..3, 4..6) == [[1,4],[2,5],[3,6]]
`zipwith(arr2, f)`: zipwith(1..3, 4..6, \a,b a+b) == [1+4, 2+5, 3+6]
`flatten(level)`: [[1,2],[3,4]] -> [1,2,3,4], if level is not provided completely flattens the array, if level n is provided flattens up to n levels
`dimensions()`: returns an array of dimensions where [n] is the nth dimension, equivalent to len() when array is 1 dimensional
`transpose()`: transposes a matrix ("flip" it on its diagonal axis)

    transpose(0..2) == [[0],
                        [1],
                        [2]]
    [[1,2],[3,4]].transpose() == [[1,3],
                                  [2,4]]

`reshape(into)`: takes a matrix and reshapes it to fit into the array of dimensions `into`, returns nil if not possible

	0..6.reshape([2,3]) == [[0,1,3],
	                        [4,5,6]] ; 2 rows of 3 columns

`overlaps(arr2)`: see if arr and arr2 have values in common
`seek(val)`: find the first index of val in arr or nil
`find(val)`: find all indexes where val can be found in arr or empty []

`map(f)`:
	1..3.map(f)    == [f(1),f(2),f(3)]
`fold(f)`:
	1..3.fold(f)   == f(f(1,2),3)
`fold(i,f)`:
	1..3.fold(9,f) == f(f(f(9,1),2),3)
`pick(f):` array of elements for which f returns true (1)
	0..6.pick(\x x%2)  == [1,3,5]
`group(f)`:
splits array into an array of arrays, one subarray for each unique result of f
	0..7.group(\x x%2) == [[0,2,4,6],[1,3,5,7]]

`dedup()`: deduplicates array
`isect(arr2)`: returns the deduplicated intersection of arr and arr2 (in both arr and arr2)
`union(arr2)`: returns the deduplicated union of arr and arr2 (in arr or in arr2)
`diff(arr2)`: returns the deduplicated difference of arr and arr2 (in arr but not in arr2)
`symdiff(arr2)`: returns the deduplicated symmetric difference of arr and arr2 (in either arr or in arr2, but not both)
`cartesian(arr2)`: returns cartesian product, [1,2].cartpro(["a","b"]) == [[1,"a"],[2,"a"],[1,"b"],[2,"b"]]

coroutine.
	`drain()`: drains the leftover into an array, may thus return []
	`take(n)`: drains at most 5 elements, but may drain less if there are not enough elements left to take thus may return []
	`take_or_nil(n)`: drains exactly 5 elements, or returns nil if not possible

Matrices are vectors of vectors. Since in Ananke vectors are written in rows `[1,2,3]` (after all, the way text files are laid out in memory is not conductive to parsing in columns), matrices are written as vectors of rows: `[[1,2,3],[4,5,6]]` defines a `2x3` matrix, meaning 2 vectors of 3 long, so is equal to `1..6.reshape(2,3)`. To index a matrix, `m[vec, coord]` where `vec` is the vector and `coord` is the coordinate of that vector. Apparently this is called "row major form" because one can also see it as `m[row, col]`. Matrix multiplications happen left to right in this language.

matrix.
	`det()`: determinant
	`diag()`: diagonalize
	`eigen()`: eigenvector