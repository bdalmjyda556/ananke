#include <stdlib.h>
#include <string.h>

typedef struct type {
	// integer and floats (i32, f64, etc)
	// how many bits does an instance of this type take
	// ptr, dptr, xptr, mem sizes are all materialized into `bits`
	// for functions and composition types: amount of arguments/fields
	// for arrays: the length
	int size;
	
	// qualifiers
	unsigned
		q_exact:1,
		q_volat:1,
		q_mut:1,
		q_pure:1,
		q_atom:1,
		q_ftz_daz:1, // floats
		q_signed:1, // ints
		q_bigend:1; // little end if 0, "native" endian gets materialized as either choice right away
	enum {o_overflow, o_undefined, o_saturate} overflow:2;
	enum {l_static, l_thread, l_heap, l_stack} loc:2;
	enum {k_sum, k_prod, k_array, k_vector, k_hash, k_int, k_float, k_ptr, k_fn, k_prop} kind:4;

	void *predicate;

	// compositions, function args
	int *fields;
	char **fieldnames;

	// function returns
	int *rets;
	int num_rets;

	// pointers, arrays
	int elemtype;
} Type;


#define TYPESTORE_STARTING_CAP 512
static Type _typestore[TYPESTORE_STARTING_CAP];
static Type *typetab = _typestore;

static Type *alloc_type(){
	static int len = 0;
	static int cap = TYPESTORE_STARTING_CAP;
	if (len >= cap){
		int oldcap = cap;
		cap *= 2;
		if (oldcap == TYPESTORE_STARTING_CAP){
			Type *tmp = malloc(2*cap*sizeof(Type));
			memcpy(tmp, typetab, oldcap*sizeof(Type));
		} else {
			typetab = realloc(typetab, cap*sizeof(Type));
		}
	}
	return &typetab[len++];
}

#include <stdio.h>
int main(){
	Type *t = alloc_type();
	*t = (Type){.overflow = o_saturate};
	printf("%d\n",sizeof *t);
}
