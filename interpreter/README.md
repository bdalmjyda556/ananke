Checking out the source? Start in main.c, and follow the includes.

main.c is the "standalone" interpreter, while repl.c is the interactive interpreter. They both serve as front ends for parser.c, which is the real hero of this story.

To compile:
 - gcc main.c
 - gcc repl.c -lreadline
