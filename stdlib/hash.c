#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

unsigned sum(char *s){
	unsigned h=0;
	while(*s){
		h += *s;
		s++;
	}
	return h;
}

unsigned fnv(char *s){
	unsigned h = 2166136261; // FNV offset basis

	while(*s){
		h ^= *s;
		h *= 16777619; // FNV prime
		s++;
	}
	return h;
}

unsigned mh2(char *s){
	const unsigned int m = 0x5bd1e995;
	const int r = 24;
	const int seed = 0;
	int len = strlen(s);
	// Initialize the hash to a 'random' value

	unsigned int h = seed ^ len;

	// Mix 4 bytes at a time into the hash

	const unsigned char * data = (const unsigned char *)s;

	while(len >= 4)
	{
		unsigned int k = *(unsigned int *)data;

		k *= m; 
		k ^= k >> r; 
		k *= m; 
		
		h *= m; 
		h ^= k;

		data += 4;
		len -= 4;
	}
	
	// Handle the last few bytes of the input array

	switch(len)
	{
	case 3: h ^= data[2] << 16;
	case 2: h ^= data[1] << 8;
	case 1: h ^= data[0];
	        h *= m;
	};

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}

int comp(const void *a, const void *b){
	const int x = *(int*)a;
	const int y = *(int*)b;
	return x>y;
}

// divide by this to uniformly quantize 32 bit nums into 8 bit nums
const int d = (1<<(32-8)); // 2^24

int main(){
	FILE *f = fopen("words.txt", "rb");
	char buf[128] = {0};
	int counts[3][256] = {0};
	int c=0;
	while (fgets(buf,128,f) != NULL){
		counts[0][fnv(buf)/d]++;
		counts[1][sum(buf)/d]++;
		counts[2][mh2(buf)/d]++;
		c++;
	}

	qsort(counts[0], 256, sizeof(int), &comp);
	qsort(counts[1], 256, sizeof(int), &comp);
	qsort(counts[2], 256, sizeof(int), &comp);

	printf("Value\tFNV-1a\tsum\tMurmurHash2\n");
	for (int i = 0; i < 256; ++i)
	{
		printf("%03d\t%d\t%d\t%d\n", i, counts[0][i], counts[1][i], counts[2][i]);
	}
	printf("hashed %d words.\n", c);
}