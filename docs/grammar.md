# Braces

	{ x } scoped expression
	[ x ] array, index, template
	( x ) expression, call
	<| x |> reserved
	{| x |} reserved
	[| x |] reserved
	(| x |) reserved
	<: x :> reserved
	{: x :} reserved
	[: x :] reserved
	(: x :) reserved

# Operators

* A **monadic** operator has a single operand and is either prefix or postfix.
* A **dyadic** operator has 2 operands and is always infix.

Precedence 0 (will be evaluated first):
 * `#expr`: macro invocation

Precedence 1:
 * `(expr)`
 * `{expr}`

Precedence 2:
 * `a[b]`: array access
 * `a.b`: member access or uniform function call
 * `a$`: pointer dereference
 * `a@`: address of
 * `a?`: safe operator
 * `a??b`: coalescence operator `nil??a -> a, error??a -> a`

Precedence 3:
 * `a to b`: converting cast
 * `a as b`: bit reinterpretation cast

Precedence 4:
 * `a^b`: exponentiate
 * `-a`: negate
 * `!a`: bitwise not
 * `a++`: increment a but return old value
 * `a--`: decrement a but return old value
 * any user created monadic operator

Precedence 5:
 * `a*b`: multiply (or in types: pointer type)
 * `a/b`: divide (or in types: discriminated union)
 * `a%b`: remainder with sign of b
 * `a%%b`: remainder with sign of a
 * `a/%b`: returns both remainder as well as quotient

Precedence 9:
 * `a+b`: add
 * `a-b`: subtract
 * `a..b`: concatenate (or in the case of two numerical operands, generate vector spanning values a to b)
 * any user created dyadic operator

Precedence 6:
 * `a<<b`: shift left
 * `a>>b`: shift right
 * `a<<<b`: rotate left
 * `a>>>b`: rotate right

Precedence 7:
 * `a&b`: bitwise and

Precedence 8:
 * `a~b`: bitwise xor
 * `a|b`: bitwise or

Precedence 10:
 * `a in b`: containment
 * `a is b`: type of a is b
 * `a == b`: equal to
 * `a != b`: not equal to
 * `a > b`: greater than
 * `a < b`: less than
 * `a >= b`: greater than or equal to
 * `a <= b`: less than or equal to
 * `a <=> b`: compare: returns 0 if equal, 1 if a > b, -1 if a < b.

Precedence 11:
 * `not a`: logical not

Precedence 12:
 * `a and b`: logical and

 Precedence:
 * `a xor b`: logical xor (either a or b, but not both)
 * `a or b`: logical or

Precedence:
 * `then`
 * `until`
 * `of`

Precedence 13:
 * `a = b`: assignment

Reserved (will not compile):
 * `a :: b`
 * `a // b`
 * `a ~> b`
 * `a <~ b`
 * ``` `a```

## Assignment operator composition

Dyadic operators can be composed with the assignment operator:

`a = a + b` can be composed into `a += b`. `a X= b` is always equivalent to `a = a X b` and vice versa where `X` is a dyadic operator, with the following exceptions: X can not be any of `<, >`. This is because `>=`, `<=` are already comparison operators. The composed operator takes the precedence of normal assignment (`=`).

### Examples

	-a^b == -(a^b)
	a^-b == a^(-b)
	a^b*c == (a^b)*c
	a^b^c == a^(b^c)
    not a and not b or c == ((not a) and (not b)) or c


# Grammar

Note: in flux

```
{x} (x (","x)*)
[x] (x)?

usrtype: type declared earlier by the user
usrvar: variable declared earlier by the user
usrop: operator declared earlier by the user
ident: regex here

literal:
	floatlit: regex here
	intlit: regex here
	strlit: regex here
	null true false

operator:
	usrop

typemod:
	const volatile extern export static

type:
	regex here (i32, f32)
	usrtype
	void bool str
	var ref let

	prodtype: '{'{type ident}'}'
	sumtype: '{'type ident ('/'type ident)+'}'
	arrtype: type'['('%'|'.'|'0')?expr?']'
	compound: type type+

kind:       type|"type"|"expr"|"life"
fn:         "fn"|"co"
body:       "{" expr* "}"
for-is:     "for" ident "=" expr "," expr ["," expr] body
for-in:     "for" {ident} "in" expr body
while-loop: "while" expr body 
do-loop:    "do" [expr] body ["while" expr]
if-cond:    "if" expr body ("elif" expr body)* ["else" body]
case-cond:  "case" expr "{" {expr":"expr} "}"
var-decl:   type ident
var-defn:   var-decl "=" expr
kind-decl:  kind ident
fn-decl:    fn ident "(" {decl}? ")"
fn-defn:    fn-decl body
lambda:     "\"("\"|{ident}) expr
```
