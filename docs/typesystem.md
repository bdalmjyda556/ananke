# Primitives
These keywords consists of a type, bitsize and optional modifiers.

Schema: `xy[z]` with x=type, y=size, z=optional modifier

Types:

	i:      signed (two's complement) integers
	u:      unsigned integers
	f:      IEEE754 floating point numbers

Sizes:

	[0-9]+: amount of bits as specified
	dptr:   amount of bits to store an address to data on the target system
	xptr:   amount of bits to store an address to (executable) code on the target system
	ptr:    max(dptr,xptr)
	mem:    amount of bits to store the numerical length of the maximum continuous allocation on the target system in octets

Modifiers:

	u:      overflow/wrapping is UB
	s:      overflow/wrapping saturates
	n:      floating point denormals are flushed to/treated as zero (FTZ+DAZ flags)

* Default int behaviour: overflow wraps

Examples: u80, i32u, umems, u24, f256, u3u f32n, i17

Special cases:

* `void` is used for optional types and raw pointers. It is 0 bits. Its "value" is always `nil`.
* `bool` is an alias for `u1`. Its value is either `true`(1) or `false`(0).
* `str` is a string.
* `fun` is an executable variable (function or closure).
* above primitives, with exception of `u1` but not `bool`, are "named primitives" and may not have modifiers or sizes.
* types such as u1 may be padded with extra bits to nicely fit into the native word size. Use `exact` to prevent this.
* `f` primitives can have a value of `inf`, `-inf`, `nan`, `-nan`. The `nan`s have multiple possible bit representations.

# Symbol qualifiers
These apply to symbols, such as variables.

Storage location:

	static:     somewhere where it's available for the lifetime of the program
	thread:     static but for thread lifetimes. Also a unique copy per thread (thread local)
	dyn:        on the heap with a dynamic (inferred) lifetime (garbage collector)

Default: stack. It will do escape analysis, ownership analysis to determine wether something can be put in the heap, stack, static, before relenting it to the garbage collection. If no garbage collection is necesary, it will not be a part of the compiled binary. If it is necesary, the compiler generates a notice.

# Type qualifiers
These apply to types.

layout:

	exact:
		on code:      no instruction reordering
		on types:     no padding or extra aligning, exactly n bits instead of "at least n bits"

storage layout:

	litend:           little endian octet order
	bigend:           big endian octer order

Default storage layout is whatever the target system default is, which may be different from both `litend` and `bigend`. When storage layout modifiers are applyed to a container type, it applies to each member individually.

behaviour:

	var:              modifiable (the default is immutable/const)
	pure:             assume function has no side effects and anything to do with it can be freely reordered.
	volatile:         assume value is unknowable & writing has side effects, keep the relative order of volatile reads/writes
	atom:
	    on types:     use mutexes or lock instructions for read/writes
	    on functions: force calls to be synchronous via use of guards (mutexes)

Default is constant (immutable). No need to specify `let` when specifying another qualifier.

predicates:

	T where expr       Syntax sugar for assert(expr) at every write.
	T assume expr      Same optimizations but no assert is generated.

	type digit = u8 where this<10 /. 0 thru 9 ./
	type even<T>  = T where this%2==0 /. all even naturals ./

# Illegal combinations

* more than 1 storage location specified
* more than 1 storage layout specified
* volatile + pure
* volatile + predicate 
* storage location + function
* usage of `eval` inside of a compound type
* usage of storage location specifier inside of a compound type

# Container types

Ananke has sets, enums, unions, product types (and tuples).
`,` tuple/product type seperator (all of)
`|` `/` enum/sum type seperator (exactly one of)

`,` Has a lower precedence than the others, so `a,b|c,d` is parsed as `a,(b|c),d`. The others have equal precedence, so don't be afraid to use braces.

Examples: *note* syntax to be revised

	typ article = 
		string title,
		string author,
		date|void published, /. article may not have a publishing date ./
		pinned?hidden?locked flags /. could be any combination, hence a set ./

	typ article = 
		title of string,
		author of string,
		published of date | void,
		flags of pinned ? hidden ? locked

	let article = struct{
		title: string,
		author: string,
		published: enum{date,void},
		flags: set{pinned,hidden,locked}
	}
