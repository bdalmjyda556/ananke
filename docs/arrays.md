# Arrays, vectors, matrices, hash tables, search trees

# Warning: incomplete. Exact naming of methods is still not settled. Neither is declaration order, i.e. `i32[3]` vs `[3]i32`. The big picture has been settled though.

## Synopsis

	[]i32     a = [1,2,3]     // array of i32
	[0]i32    v = [1,2,3]     // implicit vector of i32
	[3]i32    w = [1,2,3]     // explicit vector of 3 i32
	[2,3]i32  m = [[1,2,3],   // 2x3 matrix of i32
	               [4,5,6]]
	[%str]i32 h = ["one":1]   // hash table of i32 indexed by str
	[.str]i32 h = ["one":1]   // binary search tree of i32 indexed by str

## Arrays

Arrays are easiest to work with. They are sized dynamically, so you can always insert new elements at the end. Good for data which you don't know the size of beforehand.

	[]*fn(u16)u8 x // declare x as an array of pointers to functions of u16 returning u8

## Vectors

Vectors are fastest to work with. They have a fixed size, so you can not grow or shrink a vector. You can however assign the product of an operator on one or more vectors into a new vector. Say you concatenate vector a and b and store the result in new implicit vector c.

	// type []i32 is infered, we only need to specify a vector
	[0] a = [1,2,3]
	[0] b = [4,5,6,7]
	[0] c = a..b
	// c == [1,2,3,4,5,6,7]

## Vector of vectors

In order to make a multidimensional array, you _could_
	
	[3][9]i32 y    // declare y as 3 vectors of 9 i32s
	y[1][2]        // acces "row" 1 and "column" 2

## Matrices

Or you can use matrix notation

	[3,9]i32 z     // declare z as a 3 by 9 matrix of i32
	z[1,2]         // access row 1 col 2

The same `[0]` trick with vectors works with matrices

	[0,0] actually2by3 = [[1,2,3],[4,5,6]]

## Hash tables

both `[%]` and `[.]` are a key/value stores, also known as dictionaries or associative arrays. But hash tables `[%]` have different characteristics than binary search trees `[.]`, so choose carefully. For instance, `[.]`s guarantee to keep the order of inserted elements, whereas `[%]`s don't. In general, `[%]`s are faster, but `[.]` might serve a specific use case.

	[%str]i32 ht = ["hello":5, "world":25]
	assert(ht["world"] == 25)

## Operators

### Concatenate

To glue two arrays together, use the concatenate (`..`) operator:

	assert( [1,2]..[3,4] == [1,2,3,4] )

### Matrix multiply

To multiply 2 matrices or vectors as one would in linear algebra, use the `dot` operator

	asert( [1,2] dot [3,4] == 11 )

### Component wise overloading

Ananke allows for natural linear algebra: an operator applied to any 2 arrays of the same dimensions will be applied component wise:

	assert( [1,2,3] - [1,1,1] == [0,1,2] )
	assert( [1,2,3] + [6,5,4] == [7,7,7] )
	assert( [10,10,10] / [2,5,10] == [5,2,1] )

You can also operate on an array with a single value:

	assert( [1,2,3] - 1 == [0,1,2] )
	assert( [1,2,3] * 3 == [3,6,9] )

## Core methods

`a.len()`: How many items the array has

`a.cap()`: How many items the array can fit

`a.size()`: How many bytes of memory the array takes up

### Building and destroying

`a.insert(item,index)`: create a copy of `a` with `item` inserted at `a[index]`. If there was something there, the rest of the copy is moved along.

`a.delete(index)`: same semantics but this time the item at `index` is deleted.

`a.push()`: insert before first elem

`a.add()`: insert after last elem

`a.shift()`: remove first elem

`a.shift(n)`: remove first n elems

`a.pop()`: remove last elem

`a.pop(n)`: remove last n elems

### Map, filter, fold, zip

`a.map(f)`: For each element e of a copy of a, e = f(e)

	[1..5].map(++) == [2,3,4,5,6]

`a.filter(f)`: For each element of an array, keep or discard it based on a callable of arity 1 applied to that element.

	even = fn(i32 x)bool {
		return x%2 == 0
	}
	assert( [2,3,4,5,6].filter(even) == [2,4,6] )

`a.fold(f)`: Given `f` is a dyadic callable, reduces `a` to a single thing.

	[2,4,6].fold(+) == 12 // 2+4+6
	[2,4,6,8].fold(f)     // f(f(f(2, 4), 6), 8)

`a.zip(b, f)`: Given `a.len() == b.len()`, returns a single array where each element is constructed by applying the 2-callable `func` to each pair of elements from the two arrays.

	assert( [1,2,3].zip([4,5,6], +) == [5,7,9] )
	// [1+4, 2+5, 3+6]

### Finding and matching

`a.get(b)`: interprets b as a collection of indices to take from a.

	[1,2,3,4].get([0,1,3]) == [1,2,4]
	
`a.overlap(b)`: returns the indices of a where it holds that `b.contains(a[i])`

	[1,2,3].overlap([1,1,1,2,5]) == [0,1]
	[1,2].overlap([3,4]) == []

`a.overlaps(b)`: like `overlap`, but returns just a boolean result wether any overlap exists or not.

`a.contains(needle)`: returns a boolean result wether a has an element of value needle.

`a.find(needle)`: return array of indices at which needle can be found

`a.seek(needle)`: return the first index at which needle can be found

### Statistical methods

`a.min()`: gets the smallest value present in `a`

`a.max()`: gets the largest value present in `a`

	assert( [1,2,3].max() == 3 && [1,2,3].min() == 1 )

`a.avg()`: average value (aka mean)

`a.median()`: middle value (only works properly when the array is sorted - at the programmer's discretion)

`a.mode()`: most common value

`a.stddev()`: standard deviation ( 1/N )

`a.corstddev()`: corrected standard deviation ( 1/(N-1) )

`a.quartiles()`: returns [Q0,Q1,Q2,Q3,Q4] aka [minimum, first quartile, median, third quartile, maximum]. Useful for box plots.

`a.sample()`, `a.sample(n)`: picks 1(n) random element(s) from `a` (useful for sampling repeating picks)

`a.shuffle()`: shuffles `a` (combined with a.pop() or a.shift(): useful for sampling unique picks)

`a.hamming(b)`: hamming distance between `a` and `b`

### Boolean methods

`a.mask()`, `a.mask(n)`: Makes a new array (of length a.max() or n) and interprets `a` as a collection of indices which should be `true`

	assert( [0,1,4,6].mask() == [1,1,0,0,1,0,1] )
	// [0], [1], [4], [6] are 1 the rest is 0
	assert( [].mask(4) == [0,0,0,0] )
	// empty array -> no indices are 1

`a.invert()`: truthy elements are made 0, falsy elements are made 1

### Shaping

`a.reverse()`: reverse `a`

`m.transpose()`: transposes the matrix m

`a.weave(b)`: interleaves `a` with `b`

	[1,3].weave([2,4]) == [1,2,3,4]
	[2,4].weave([1,3]) == [2,1,4,3]

`a.mate(b)`: turns `a` and `b` into an array of pairs (mates)

	[1,3].mate([2,4]) == [[1,2],[3,4]]

`a.rotate(offset, axis)`: Rotate (in the sense of bitwise rotation)

	a = [1,2,3,4].rotate(-1)
	// a[0] becomes x[0 -1] (evals to x[x.len()])
	// a == [2,3,4,1]

	m = [[1,2,3],
	     [4,5,6],
	     [7,8,9]]
	    .rotate(1,1)
	    // offset 1, axis 1 (x=0, y=1, z=2, fourth dimension=3, fifth=4, etc)

	assert(
	m == [[7,8,9],
	      [1,2,3],
	      [4,5,6]]
	)

`a.dimensions()`

	d = [[1,2,3],[4,5,6]].dimensions()
	assert(d == [2,3]) // 2 rows of 3 collumns

`a.reshape(into)`:

	m = [1,2,3,4,5,6].reshape([2,3])
	assert(m == [[1,2,3],[4,5,6]])

`a.flatten()`:

	assert( [[1,2],[3,4]].flatten() == [1,2,3,4] )
