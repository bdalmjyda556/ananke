# related keywords

* `fn(in)ret`: a function taking parameters `in` and returning something of type `ret`
* `op`: operator type. See section operators.
* `return`: return to caller context

# function

Myfunc is a function that takes a pointer to an array of i32, a pointer to an i8, and returns a pointer to an array of i32.

	fn myfunc(i32[]* x, i8* y) i32[]* {
		// code	using x and y
	}

# operators

* lbp: left binding power
* rbp: right binding power
* lhs: left hand side operand
* rhs: right hand side operand
* res: type of result

```
	mybinop  = op lbp (lhs) rbp (rhs) res {}
	mypreop  = op lbp () rbp (rhs) res {}
	mypostop = op lbp (lhs) rbp () res {}
```

# overloading

Define operators, functions or coroutines for more than 1 input type.

	op (lhs) + (rhs) {}
	op - (rhs) {}
	op (lhs) ++ {}
	fn foo (args) res {}
