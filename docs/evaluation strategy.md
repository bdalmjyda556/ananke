Ananke is not eveluated exactly as specified, unless the `exact` keyword is used. But the compiler makes sure it won't cause any side effects.

# Exact code

	{
		x = 5
		y = x+x
		z = y+y
		a = 4
		b = a+a
		c = a+x
	}

The compiler is free to reorder the expressions in that example, unless they affect one another.

	exact { 
		x = 5
		...
	}

This block, Ananke will execute exactly as specified

# Calls

Function arguments must always feel like they are pass-by-value. But it is not so under the hood! Ananke may pass by name or by reference if it would benefit performance.

	f = fn(i32 a, i32 b, i32 d, []i32 d){
		(a+b+c)*d
	}

	x = 5
	f(1+1, 4, x, [1,2,3,4])

In that example, 1+1 and 4 will be passed by value, x will be passed by name, [1,2,3,4] will be passed by reference. If the function f had modified x, then x would have been passed by value.

In essense, Ananke is allowed to make specialized function instances per call site.