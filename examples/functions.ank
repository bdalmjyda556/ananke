/.functions
- uniform call syntax
- multiple returns
- lambdas
- capture by default (use the fn[] for by-reference)
- pass by value (pointers replace references)
./

; uniform call syntax: both method(obj) and obj.method()
fn f(i32 x){x>3}
check f(4) and 4.f()

; multiple returns
fn g(){return 1,2}
let a,b = g()
check a==1 and b==2

; lambdas are a shorthand notation
let h = \x x+6 ; fn(x){x+6}
check h(0) == 6

let i = \\printl("this \\lambda takes no input")
i()

; captures and arguments
var x = 0
let y = 4
fn f[x](){
	x++ ; modify the original x
	y++ ; modify our local copy of y
	return y
}
check f()==5 and y==4 and x==1

; we can modify any original through a pointer
fn g(i32* a) void {
	a$ = 5
}
i32 z = 3
g(z@)
check z == 5